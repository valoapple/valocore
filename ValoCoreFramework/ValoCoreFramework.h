//
//  ValoCoreFramework.h
//  ValoCoreFramework
//
//  Created by Mikhail Savchenko on 18.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for ValoCoreFramework.
FOUNDATION_EXPORT double ValoCoreFrameworkVersionNumber;

//! Project version string for ValoCoreFramework.
FOUNDATION_EXPORT const unsigned char ValoCoreFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ValoCoreFramework/PublicHeader.h>


