//
//  VALONodeTypeService.h
//  ValoCore
//
//  Created by Kirill Shalankin on 20/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALONodeTypeService : NSObject

+ (NSInteger)nodeTypeWithName:(NSString *)text;

@end
