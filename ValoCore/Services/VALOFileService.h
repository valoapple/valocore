//
//  VALOFileService.h
//  ValoCore
//
//  Created by Mike Fluff on 17.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VALONodeService,Rsync;
@interface VALOFileService : NSObject

@property (nonatomic, strong) VALONodeService *nodeService;
@property (nonatomic, strong) Rsync *rsync;

-(void)saveNodeWithFile:(NSString *)file nodeID:(NSString *)nodeID;
-(NSString *)getFileParentID:(NSString *)file;
-(NSDate *)getModifiedDateByPath:(NSString *)file;
-(NSArray *)namesArrayFromPath:(NSString *)path;
-(NSArray *)namesArrayToDateFromPath:(NSString *)path recentDate:(NSDate *)date;
-(NSString *)getFileNodeID:(NSString *)file;
-(NSSet *)checkRemoteFilesToDelete:(NSString *)path parentID:(NSString *)parentID;
-(NSSet *)checkRemoteFilesToAdd:(NSString *)path parentID:(NSString *)parentID;
-(NSSet *)checkRemoteFilesToProcess:(NSString *)path parentID:(NSString *)parentID;
-(NSString *)getFileVersionID:(NSString *)file;
-(NSString *)createSignaturePath:(NSString *)file;
-(NSString *)getRenamedNodeID:(NSString *)file;
-(void)createDirectory:(NSString *)path;
-(void)createRootNode:(NSString *)nodeID path:(NSString *)path;

-(NSString *)createPathFromParentsIdsArray:(NSArray *)parents withName:(NSString *)name;

-(BOOL)checkFSPathExistsForPath:(NSString *)path;
-(BOOL)checkPathIsFile:(NSString *)file;
-(BOOL)saveData:(NSData *)data toFile:(NSString *)file;

-(BOOL)movePathToPath:(NSString *)oldPath newPath:(NSString *)newPath;
-(BOOL)deletePath:(NSString *)path;

-(NSString *)findRenamedFileAction:(NSString *)file;


//Rsync related stuff
-(NSString *)createDeltaFromFile:(NSString *)file withSignature:(NSString *)signature;
-(NSString *)createSignatureFromFile:(NSString *)file;
-(BOOL)patchFile:(NSString *)file withDelta:(NSString *)delta;
-(NSString *)createDeltaPath:(NSString *)file;
-(void)cleanup:(NSString *)file;

@end
