//
//  VALOActivitiesService.h
//  ValoCore
//
//  Created by Mike Fluff on 27.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VALONodeService,VALOFileService;
@interface VALOActivitiesService : NSObject

@property (nonatomic, strong) VALONodeService *nodeService;
@property (nonatomic, strong) VALOFileService *fileService;
@property (nonatomic, strong) NSString *accessToken;

-(BOOL)confirmCreateItem:(NSDictionary *)activity;
-(NSArray *)rebuildActivity:(NSArray *)activities;

@end
