//
//  VALOLoginService.m
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//
#if TARGET_OS_IOS
#import <UIKit/UIKit.h>
#endif

#import "VALOLoginService.h"
#import "VALOLoginOperation.h"
#import "VALOCreateDriveOperation.h"
#import "VALOUserDefaultsKeyHolder.h"
#import "PDKeychainBindings.h"
#import "VALONotificationCenterNameHolder.h"
#import "VALOUserService.h"
#import "VALODeviceService.h"


static NSString *const kVALOiPhoneType    = @"ios_mobile";
static NSString *const kVALOiPadType      = @"ios_tablet";

@interface VALOLoginService ()

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *tmpToken;


@end

@implementation VALOLoginService

- (instancetype)initWithEmail:(NSString *)email password:(NSString *)password {
    self = [super init];
    if (self) {
        _email = email;
        _password = password;
    }
    return self;
}

#pragma mark - Requests

- (void)loginOperation {
    VALOLoginOperation *loginOperation = [[VALOLoginOperation alloc] initWithEmail:self.email password:self.password];
    
    [loginOperation addOperationWithSuccess:^(NSDictionary *userJSON, NSString *deviceID, NSString *token) {
        [self saveAccessToken:token];
        [self saveDeviceID:deviceID];
        [self successfulAttemptToLogin];
        [self stopActivityIndicator];
        [self saveUserWithJSON:userJSON];
        
    } failure:^(NSError *error, NSDictionary *serializedData) {
        if (serializedData[@"tmp_token"] == nil) {
            [self showAlertWithText:@"Login or password are incorrect"];
            
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                for (NSDictionary *deviceJSON in serializedData[@"devices"]) {
                    [self saveDeviceWithJSON:deviceJSON];
                }
            });
            
            NSString *deviceID = [self findDeviceID:serializedData[@"devices"]];
            
            _tmpToken = serializedData[@"tmp_token"];
            
            if ([deviceID length] != 0) {
                [self loginWithDeviceID:deviceID];
                
            } else {
                [self loginAndCreateDevice];
            }
        }
    }];
}

- (void)loginWithDeviceID:(NSString *)deviceID {
    VALOLoginOperation *loginOperation = [[VALOLoginOperation alloc] initWithEmail:self.email
                                                                          password:self.password
                                                                          deviceID:deviceID
                                                                             token:self.tmpToken];
    [loginOperation addOperationWithSuccess:^(NSDictionary *userJSON, NSString *deviceID, NSString *token) {
        [self saveAccessToken:token];
        [self saveDeviceID:deviceID];
        [self successfulAttemptToLogin];
        [self stopActivityIndicator];
        [self saveUserWithJSON:userJSON];
        
    } failure:^(NSError *error, NSDictionary *serializedData) {
        // show error
    }];
}

- (void)loginAndCreateDevice {
    VALOLoginOperation *loginOperation = [[VALOLoginOperation alloc] initWithDeviceName:[self deviceName]
                                                                           serialNumber:[self uniqueVendor]
                                                                                   type:[self deviceType]
                                                                                  token:self.tmpToken];
    [loginOperation addOperationWithSuccess:^(NSDictionary *userJSON, NSString *deviceID, NSString *token) {
        [self createDriveOperationWithDeviceID:deviceID accessToken:token];
        [self saveAccessToken:token];
        [self saveDeviceID:deviceID];
        [self successfulAttemptToLogin];
        [self stopActivityIndicator];
        [self saveUserWithJSON:userJSON];
        
    } failure:^(NSError *error, NSDictionary *serializedData) {
        // show error
    }];
}

- (void)createDriveOperationWithDeviceID:(NSString *)deviceID accessToken:(NSString *)accessToken {
    VALOCreateDriveOperation *createDriveOperation = [[VALOCreateDriveOperation alloc] initWithDeviceID:deviceID
                                                                                              driveName:[self deviceName]
                                                                                                  token:accessToken];
    [createDriveOperation addOperationWithSuccess:^(NSDictionary *response) {
        NSLog(@"Create drive operation: %@", response);
        
    } failure:^(NSError *error) {
        
    }];
}



#pragma mark - Helpers

- (NSString *)findDeviceID:(NSDictionary *)responseData {
    for (NSDictionary *item in responseData) {
        if ([[self uniqueVendor] isEqualToString:item[@"serial_number"]]) {
            return item[@"id"];
        }
    }
    return nil;
}

- (NSString *) machineModel
{
    size_t len = 0;
    sysctlbyname("hw.model", NULL, &len, NULL, 0);
    
    if (len)
    {
        char *model = malloc(len*sizeof(char));
        sysctlbyname("hw.model", model, &len, NULL, 0);
        NSString *model_ns = [NSString stringWithUTF8String:model];
        free(model);
        return model_ns;
    }
    
    return @"Just an Apple Computer"; //incase model name can't be read
}

- (NSString *)deviceType {
#if TARGET_OS_IOS
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return kVALOiPadType;
    } else {
        return kVALOiPhoneType;
    }
#endif
#if TARGET_OS_MAC
        return @"osx";
#endif
    return nil;
}


- (NSString *)deviceName {
    #if TARGET_OS_IOS
    return [UIDevice currentDevice].name;
    #endif
    #if TARGET_OS_MAC
    return [[NSHost currentHost] localizedName];
    #endif
    return nil;
}


// TODO: Change to https://github.com/kishikawakatsumi/UICKeyChainStore
- (NSString *)uniqueVendor {
    PDKeychainBindings *keychain = [PDKeychainBindings sharedKeychainBindings];
    NSString *uniqueIdentifier = [keychain objectForKey:[VALOUserDefaultsKeyHolder keychanUUIDKey]];
    
    if (!uniqueIdentifier || !uniqueIdentifier.length) {
        #if TARGET_OS_IOS
        NSUUID *udid = [[UIDevice currentDevice] identifierForVendor];
        uniqueIdentifier = [udid UUIDString];
        #endif
        #if TARGET_OS_MAC
        uniqueIdentifier = [self getMacUUID];
        #endif
        [keychain setObject:uniqueIdentifier forKey:[VALOUserDefaultsKeyHolder keychanUUIDKey]];
    }
    //NSLog(@"UUID in keychan ===> %@", uniqueIdentifier);
    return uniqueIdentifier;
}

#if TARGET_OS_MAC
- (NSString *)getMacUUID {
    struct timespec ts = { .tv_sec = 5, .tv_nsec = 0 };
    uuid_t uuid = {};
    
    if (gethostuuid(uuid, &ts) == -1) {
        switch (errno) {
            case EFAULT:
                fputs("Failed to get system UUID: unknown error", stderr);
                return nil;
            case EWOULDBLOCK:
                fputs("Failed to get system UUID: timeout expired", stderr);
                return nil;
        }
    }
    
    uuid_string_t uuid_string;
    uuid_unparse_upper(uuid, uuid_string);
    return [NSString stringWithFormat:@"%s" , uuid_string];
}
#endif

#pragma mark - NSUserDefaults

- (void)saveAccessToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:[VALOUserDefaultsKeyHolder accessTokenKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveDeviceID:(NSString *)deviceID {
    [[NSUserDefaults standardUserDefaults] setValue:deviceID forKey:[VALOUserDefaultsKeyHolder deviceIDKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Send Notification

- (void)successfulAttemptToLogin {
#if TARGET_OS_IOS
    [[NSNotificationCenter defaultCenter] postNotificationName:
     [VALONotificationCenterNameHolder successfulLoginKey] object:nil];
#endif
#if TARGET_OS_MAC
    NSString *observedObject = @"com.iqreserve.Valo";
    NSDistributedNotificationCenter *center =
    [NSDistributedNotificationCenter defaultCenter];
    [center postNotificationName: [VALONotificationCenterNameHolder successfulLoginKey]
                      object: observedObject
                    userInfo: [[NSDictionary alloc] initWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",nil] deliverImmediately: YES];
    
    
#endif
}

- (void)stopActivityIndicator {
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[VALONotificationCenterNameHolder activityStopKey] object:nil];
    
}

- (void)showAlertWithText:(NSString *)text {
    [[NSNotificationCenter defaultCenter] postNotificationName:[VALONotificationCenterNameHolder showAlertKey]
                                                        object:[NSString stringWithFormat:@"%@", text]];
}

#pragma mark - Services

- (void)saveUserWithJSON:(NSDictionary *)userJSON {
    VALOUserService *userService = [[VALOUserService alloc] init];
    
    [userService saveUserWithUserJSON:userJSON];
}

- (void)saveDeviceWithJSON:(NSDictionary *)deviceJSON {
    VALODeviceService *deviceService = [[VALODeviceService alloc] init];
    
    [deviceService saveDeviceWithUserJSON:deviceJSON];
}

@end
