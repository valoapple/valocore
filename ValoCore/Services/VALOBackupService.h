//
//  VALOBackupService.h
//  ValoCore
//
//  Created by Mike Fluff on 17.04.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VALOBackup;
@interface VALOBackupService : NSObject

@property (nonatomic , strong , readonly) id delegate;

- (instancetype)initWithDelegate:(id)delegate;
-(void)refreshBackupPaths;
-(void)setActive:(VALOBackup *)backup status:(BOOL)status;
-(NSArray *)getBackupPaths;
-(NSInteger)countBackupPaths;

@end
