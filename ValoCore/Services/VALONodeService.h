//
//  VALONodeService.h
//  Valo
//
//  Created by Kirill Shalankin on 10/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALONodeService : NSObject

@property (nonatomic , strong , readonly) id delegate;

- (instancetype)initWithDelegate:(id)delegate;

- (void)saveNodeWithNodesJSON:(NSDictionary *)nodesJSON;

- (NSArray *)childrenNodesForNodeID:(NSString *)nodeID;
- (NSArray *)recyclerNodesForNodeID:(NSString *)nodeID;

- (void)markDeletedNodeWithNodeID:(NSString *)nodeID;
- (void)updateNodeNameWithNodeName:(NSString *)name nodeID:(NSString *)nodeID;

- (void)deleteNodeId:(NSString *)nodeID;

- (void)markNodeAsShared:(NSString *)nodeID;

- (NSString *)nodeIDByNameAndParent:(NSString *)parentID nodeName:(NSString *)nodeName;

- (NSString *)parentIDForNode:(NSString *)nodeID;

- (NSString *)versionIDForNode:(NSString *)nodeID;


- (void)createNode:(NSString *)nodeID withSN:(NSNumber *)sn withSFN:(NSNumber *)sfn withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn path:(NSString *)path;

- (void)createNode:(NSString *)nodeID withName:(NSString *)name withParent:(NSString *)parentId withPath:(NSString *)path withDate:(NSString *)date isFile:(NSNumber *)isFile;

- (void)setNode:(NSString *)nodeID withSN:(NSNumber *)sn withSFN:(NSNumber *)sfn withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn path:(NSString *)path;

-(NSString *)findNodeWithSN:(NSNumber *)sn withSFN:(NSNumber *)sfn;

- (NSString *)findNodeIDwithSN:(NSNumber *)sn withSFN:(NSNumber *)sfn withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn;

- (NSString *)findNodeIDbyName:(NSString *)name withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn;


- (NSString *)findNodeIDbyName:(NSString *)name;

- (BOOL)findNodebyID:(NSString *)nodeID withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn;

- (void)setNode:(NSString *)nodeID withModifiedDate:(NSDate *)date;

- (NSDate *)findModifiedDatewithNodeID:(NSString *)nodeID;

-(NSArray *)namesArrayToDateFromParentID:(NSString *)parentID recentDate:(NSDate *)date;

-(NSArray *)namesArrayToDateFromParentID:(NSString *)parentID;

-(NSInteger)countNodesWithParentID:(NSString *)parentID;

- (NSArray *)nodeNamesWithParentID:(NSString *)parentID;

-(NSString *)nodeNameByID:(NSString *)nodeID;

-(NSString *)getNodePathByName:(NSString *)nodeName andNodeID:(NSString *)nodeID;

-(NSString *)getNodePathById:(NSString *)nodeID;

-(NSString *)inodePathForNode:(NSString *)nodeID;

- (void)updateNodePSNPFSN:(NSString *)nodeId parentID:(NSString *)parentId;

- (void)updateNodeDatesWithDate:(NSString *)date nodeID:(NSString *)nodeID;

- (BOOL)nodeInDatabase:(NSString *)nodeID;

-(BOOL)nodeIsFile:(NSString *)nodeID;

-(BOOL)nodeInProgress:(NSString *)nodeID;

- (BOOL)nodeWithNodeID:(NSString *)nodeID hasSameName:(NSString *)name;

- (void)setNode:(NSString *)nodeID progress:(BOOL)progress;
- (void)setNode:(NSString *)nodeID isFile:(BOOL)isFile;


@end
