//
//  VALOVersionService.h
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VALOVersion;
@interface VALOVersionService : NSObject

- (void)saveVersionWithVersionJSON:(NSDictionary *)versionJSON;
- (void)linkVersion:(NSString *)versionID withNode:(NSString *)nodeID;
- (BOOL)checkVersion:(NSString *)versionID;
- (BOOL)versionIsOlder:(NSDictionary *)version;
- (NSString *)hashForVerison:(NSString *)versionID;

@end
