//
//  VALODeviceService.m
//  Valo
//
//  Created by Kirill Shalankin on 11/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <FastEasyMapping/FastEasyMapping.h>
#import <MagicalRecord/MagicalRecord.h>

#import "VALODeviceService.h"
#import "VALODevice.h"

@interface VALODeviceService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALODeviceService

- (instancetype)init {
    self = [super init];
    if (self) {
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    }
    return self;
}

- (void)saveDeviceWithUserJSON:(NSDictionary *)deviceJSON {
    if (![self deviceInDatabase:deviceJSON[@"id"]]) {
        FEMMapping *mapping = [VALODevice defaultMapping];
        
        [FEMDeserializer objectFromRepresentation:deviceJSON
                                          mapping:mapping
                                          context:self.localContext];
        
        [self.localContext MR_saveToPersistentStoreAndWait];
    }
}

#pragma mark - Private

- (BOOL)deviceInDatabase:(NSString *)deviceID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", deviceID];
    
    if ([VALODevice MR_findAllWithPredicate:predicate inContext:_localContext].count == 1) {
        return YES;
    }

    return NO;
}

#pragma mark - getters

- (NSArray *)allDevicesInDatabase {
    return [VALODevice MR_findAllSortedBy:@"name" ascending:YES];
}

-(NSString *)selfDeviceId {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@", [[NSHost currentHost] localizedName]];
    VALODevice *device = [VALODevice MR_findFirstWithPredicate:predicate inContext:_localContext];
    return device.id;
}


@end
