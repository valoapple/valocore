//
//  VALODriveService.m
//  Valo
//
//  Created by Kirill Shalankin on 11/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <FastEasyMapping/FastEasyMapping.h>
#import <MagicalRecord/MagicalRecord.h>

#import "VALODriveService.h"
#import "VALODrive.h"

@interface VALODriveService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALODriveService

- (instancetype)init {
    self = [super init];
    if (self) {
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];

    }
    return self;
}

- (void)saveDriveWithUserJSON:(NSDictionary *)driveJSON {
    if (![self driveInDatabase:driveJSON[@"id"]]) {
        FEMMapping *mapping = [VALODrive defaultMapping];
        
        [FEMDeserializer objectFromRepresentation:driveJSON
                                          mapping:mapping
                                          context:self.localContext];
        
        [self.localContext MR_saveToPersistentStoreAndWait];
    }
}

#pragma mark - Private

- (BOOL)driveInDatabase:(NSString *)driveID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", driveID];
    
    if ([VALODrive MR_findAllWithPredicate:predicate inContext:_localContext].count == 1) {
        return YES;
    }
    
    return NO;
}

- (NSArray *)getDrivesForDeviceID:(NSString *)deviceID {
    return [VALODrive MR_findByAttribute:@"deviceId" withValue:deviceID andOrderBy:@"driveName" ascending:YES];
}

@end
