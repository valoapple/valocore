//
//  VALOUserService.m
//  Valo
//
//  Created by Kirill Shalankin on 11/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <FastEasyMapping/FastEasyMapping.h>
#import <MagicalRecord/MagicalRecord.h>

#import "VALOUserService.h"
#import "VALOUser.h"

@interface VALOUserService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALOUserService

- (instancetype)init {
    self = [super init];
    if (self) {
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    }
    return self;
}

- (void)saveUserWithUserJSON:(NSDictionary *)userJSON {
    if (![self userInDatabase:userJSON[@"id"]]) {
        FEMMapping *mapping = [VALOUser defaultMapping];
        
        [FEMDeserializer objectFromRepresentation:userJSON
                                          mapping:mapping
                                          context:self.localContext];
        
        [self.localContext MR_saveToPersistentStoreAndWait];
    }
}

- (NSString *)getFirstName {
    return [[[VALOUser MR_findAll] firstObject] firstName];
}

- (NSString *)getLastName {
    return [[[VALOUser MR_findAll] firstObject] lastName];
}

- (NSString *)getEmail {
    return [[[VALOUser MR_findAll] firstObject] email];
}

- (NSString *)getUserID {
    return [[[VALOUser MR_findAll] firstObject] id];
}

#pragma mark - Private

- (BOOL)userInDatabase:(NSString *)userID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", userID];
    
    if ([VALOUser MR_findAllWithPredicate:predicate inContext:_localContext].count == 1) {
        return YES;
    }
    return NO;
}

@end
