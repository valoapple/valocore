//
//  VALOFileService.m
//  ValoCore
//
//  Created by Mike Fluff on 17.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOFileService.h"
#import "VALONodeService.h"
#import "RSync.h"
#import "VALOUserService.h"

@implementation VALOFileService

- (instancetype)init {
    self = [super init];
    if (self) {
        _nodeService = [[VALONodeService alloc] init];
        _rsync = [[Rsync alloc] init];
    }
    return self;
}

- (void)createRootNode:(NSString *)nodeID path:(NSString *)path {
    if(![_nodeService nodeInDatabase:nodeID])
    {
        NSError *error;
        NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
        NSString *directory = [path stringByDeletingLastPathComponent];
        NSDictionary * attrs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:directory error:&error];
        [_nodeService createNode:nodeID withSN:[attrs valueForKey:@"NSFileSystemNumber"] withSFN:[attrs valueForKey:@"NSFileSystemFileNumber"] withPSN:[attrs2 valueForKey:@"NSFileSystemNumber"] withPSFN:[attrs2 valueForKey:@"NSFileSystemFileNumber"] path:path];
        
    }
}

-(NSDate *)getModifiedDateByPath:(NSString *)file {
    NSError *error;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
    if (attrs && !error)
    {
        NSDate *date = [attrs valueForKey:@"NSFileModificationDate"];
        return date;
    }
    return nil;
}


-(NSArray *)namesArrayFromPath:(NSString *)path {
    NSError *error;
    //NSMutableArray *filesArray = [[NSMutableArray alloc] init];
    NSArray  *FSChildren = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:path error:&error];
    /*for(NSString *name in FSChildren) {
        NSString *newpath = [path stringByAppendingPathComponent:name];
        [filesArray addObject:[NSString stringWithFormat:@"%@ = %@",name,[self getModifiedDateByPath:newpath]]];
    }*/
    return FSChildren;
    
}

-(NSSet *)checkRemoteFilesToDelete:(NSString *)path parentID:(NSString *)parentID {
    NSError *error;
    NSArray  *FSChildren = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:path error:&error];
    NSArray *DBChildren = [_nodeService namesArrayToDateFromParentID:parentID];
    NSMutableSet *nodeNames = [NSMutableSet setWithArray:DBChildren];
    NSSet *fileNames = [NSSet setWithArray:FSChildren];
    [nodeNames minusSet:fileNames];
    return nodeNames;
}

-(NSSet *)checkRemoteFilesToAdd:(NSString *)path parentID:(NSString *)parentID {
    NSError *error;
    NSArray  *FSChildren = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:path error:&error];
    NSArray *DBChildren = [_nodeService namesArrayToDateFromParentID:parentID];
    NSSet *nodeNames = [NSSet setWithArray:DBChildren];
    NSMutableSet *fileNames = [NSMutableSet setWithArray:FSChildren];
    [fileNames minusSet:nodeNames];
    return fileNames;

}

-(NSSet *)checkRemoteFilesToProcess:(NSString *)path parentID:(NSString *)parentID {
    NSError *error;
    NSArray  *FSChildren = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:path error:&error];
    NSArray *DBChildren = [_nodeService namesArrayToDateFromParentID:parentID];
    NSSet *nodeNames = [NSSet setWithArray:DBChildren];
    NSMutableSet *fileNames = [NSMutableSet setWithArray:FSChildren];
    [fileNames intersectSet:nodeNames];
    return fileNames;
}

-(NSArray *)namesArrayToDateFromPath:(NSString *)path recentDate:(NSDate *)date {
    NSError *error;
    NSMutableArray *filesArray = [[NSMutableArray alloc] init];
    NSArray  *FSChildren = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:path error:&error];
    for(NSString *name in FSChildren) {
        NSString *newpath = [path stringByAppendingPathComponent:name];
        BOOL isDirectory = NO;
        
        [[NSFileManager defaultManager] fileExistsAtPath:newpath
                                             isDirectory: &isDirectory];
        if(isDirectory)
            [filesArray addObject:name];
        else if([[self getModifiedDateByPath:newpath] compare:date] == NSOrderedDescending)
            [filesArray addObject:name];
        
    }
    return filesArray;
}

-(void)saveNodeWithFile:(NSString *)file nodeID:(NSString *)nodeID {
    NSError *error;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
    NSString *directory = [file stringByDeletingLastPathComponent];
    NSDictionary * attrs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:directory error:&error];
    
    
    if (attrs && attrs2 && !error)
    {
        [_nodeService setNode:nodeID withSN:[attrs valueForKey:@"NSFileSystemNumber"] withSFN:[attrs valueForKey:@"NSFileSystemFileNumber"] withPSN:[attrs2 valueForKey:@"NSFileSystemNumber"] withPSFN:[attrs2 valueForKey:@"NSFileSystemFileNumber"] path:file];
        NSString *parentID = [_nodeService parentIDForNode:nodeID];
        [_nodeService setNode:nodeID withModifiedDate:[attrs valueForKey:@"NSFileModificationDate"]];
        [_nodeService setNode:parentID withModifiedDate:[attrs valueForKey:@"NSFileModificationDate"]];
        
    }
    
}

-(NSString *)findRenamedFileAction:(NSString *)file {
    NSString *action;
    NSError *error;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
    NSString *directory = [file stringByDeletingLastPathComponent];
    NSDictionary * attrs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:directory error:&error];
    
    
    if (attrs && attrs2 && !error)
    {
        //first we have to check if we already have node with SN and SFN in Database and find nodeID
        NSString *nodeID = [_nodeService findNodeWithSN:[attrs valueForKey:@"NSFileSystemNumber"] withSFN:[attrs valueForKey:@"NSFileSystemFileNumber"]];
        //next we have to check that node in database registered with same path
        BOOL hasSameName = [_nodeService nodeWithNodeID:nodeID hasSameName:[file lastPathComponent]];
        if(nodeID)
        {
            
            //check if node still has same parent
            if(![_nodeService findNodebyID:nodeID withPSN:[attrs2 valueForKey:@"NSFileSystemNumber"] withPSFN:[attrs2 valueForKey:@"NSFileSystemFileNumber"]])
            {
                if(hasSameName)
                {
                    action = @"moved";
                }
                else {
                    action = @"moved_renamed";
                }
            }
            else {
                if(!hasSameName) {
                    action = @"renamed";
                }
            }
            
        }
        
    }
    return action;
}


-(void)updateNodeWithPath:(NSString *)file {
    NSError *error;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
    NSString *directory = [file stringByDeletingLastPathComponent];
    NSDictionary * attrs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:directory error:&error];
    NSString *nodeID = [self getFileNodeID:file];
    [_nodeService setNode:nodeID withSN:[attrs valueForKey:@"NSFileSystemNumber"] withSFN:[attrs valueForKey:@"NSFileSystemFileNumber"] withPSN:[attrs2 valueForKey:@"NSFileSystemNumber"] withPSFN:[attrs2 valueForKey:@"NSFileSystemFileNumber"] path:file];
    NSString *parentID = [_nodeService parentIDForNode:nodeID];
    [_nodeService setNode:nodeID withModifiedDate:[attrs valueForKey:@"NSFileModificationDate"]];
    [_nodeService setNode:parentID withModifiedDate:[attrs valueForKey:@"NSFileModificationDate"]];

}

-(NSString *)getFileParentID:(NSString *)file {
    NSError *error;
    NSString *directory = [file stringByDeletingLastPathComponent];
    NSString *parentDirectory = [directory stringByDeletingLastPathComponent];
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:directory error:&error];
    NSDictionary * attrs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:parentDirectory error:&error];
    NSString *parentID = [_nodeService findNodeIDwithSN:[attrs valueForKey:@"NSFileSystemNumber"] withSFN:[attrs valueForKey:@"NSFileSystemFileNumber"] withPSN:[attrs2 valueForKey:@"NSFileSystemNumber"] withPSFN:[attrs2 valueForKey:@"NSFileSystemFileNumber"]];
    return parentID;
}

-(NSString *)getRenamedNodeID:(NSString *)file {
    NSError *error;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
    NSString *nodeID;
    
    if (attrs && !error)
    {
        //first we have to check if we already have node with SN and SFN in Database and find nodeID
        nodeID = [_nodeService findNodeWithSN:[attrs valueForKey:@"NSFileSystemNumber"] withSFN:[attrs valueForKey:@"NSFileSystemFileNumber"]];
    }
   // if(![_nodeService nodeWithNodeID:nodeID hasSameName:[file lastPathComponent]])
        return nodeID;
   // else
    ///    return nil;
}

-(NSString *)getFileNodeID:(NSString *)file {
    NSError *error;
    NSString *directory = [file stringByDeletingLastPathComponent];
    NSDictionary * attrs2 = [[NSFileManager defaultManager] attributesOfItemAtPath:directory error:&error];
    NSString *nodeID;
    nodeID = [_nodeService findNodeIDbyName:[file lastPathComponent] withPSN:[attrs2 valueForKey:@"NSFileSystemNumber"] withPSFN:[attrs2 valueForKey:@"NSFileSystemFileNumber"]];
    if(!nodeID) {
        nodeID = [_nodeService findNodeIDbyName:[file lastPathComponent]];
    }
    return nodeID;
}

-(BOOL)checkFSPathExistsForPath:(NSString *)path {
    
    NSString *iNodepath = [_nodeService inodePathForNode:[self getFileNodeID:path]];
    BOOL isDir;
    if([[NSFileManager defaultManager] fileExistsAtPath:iNodepath isDirectory:&isDir])
        return YES;
    else
        return NO;
}

-(BOOL)checkPathIsFile:(NSString *)file {
    return [_nodeService nodeIsFile:[self getFileNodeID:file]];
}

-(NSString *)getFileVersionID:(NSString *)file {
    NSString *versionID = [_nodeService versionIDForNode:[self getFileNodeID:file]];
    return versionID;    
}

-(BOOL)saveData:(NSData *)data toFile:(NSString *)file {
    NSError *error;
    [data writeToFile:file options:NSDataWritingWithoutOverwriting error:&error];
    if(!error)
        return YES;
    else
        return NO;
}

-(void)createDirectory:(NSString *)path {
    BOOL isDir;
    NSFileManager *fileManager= [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:path isDirectory:&isDir]) {
        if(![fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:NULL]) {
            NSLog(@"Error: Create folder failed %@", path);
        }
        else {
            [self updateNodeWithPath:path];
        }
    }
    
}

-(BOOL)movePathToPath:(NSString *)oldPath newPath:(NSString *)newPath {
    NSError *error;
    NSFileManager *fileManager= [NSFileManager defaultManager];
    if(![fileManager moveItemAtPath:oldPath toPath:newPath error:&error]) {
        return NO;
    }
    else {
        [self updateNodeWithPath:newPath];
        return YES;
    }

}
-(BOOL)deletePath:(NSString *)path {
    NSError *error;
    NSFileManager *fileManager= [NSFileManager defaultManager];
    if(![fileManager removeItemAtPath:path error:&error]) {
        return NO;
    }
    else {
        return YES;
    }
}


//this method used to create paths from activities

-(NSString *)createPathFromParentsIdsArray:(NSArray *)parents withName:(NSString *)name {
    NSMutableString *path = [[NSMutableString alloc] init];
    NSMutableArray *reversedParents = [NSMutableArray arrayWithArray:[[parents reverseObjectEnumerator] allObjects]];
    [reversedParents removeLastObject];
    VALOUserService *userService = [[VALOUserService alloc] init];
    NSString *userEmail = [userService getEmail];
    NSString *syncPath = [NSString stringWithFormat:@"%@/%@/%@",NSHomeDirectory(),@"VALO-new",userEmail];
    [path appendString:syncPath];
    for(NSString *parentID in reversedParents)
    {
        NSString *pathComponent = [_nodeService nodeNameByID:parentID];
        if(pathComponent)
            [path appendFormat:@"/%@",pathComponent];
        else
            return nil;
    }
    [path appendFormat:@"/%@",name];
    return path;
}

//RSync related stuff

-(NSString *)createDeltaFromFile:(NSString *)file withSignature:(NSString *)signature {
    NSString *deltaFile = [self createDeltaPath:file];
    if(!signature)
        signature = [self createSignaturePath:file];
    if([_rsync delta:file sig:signature delta:deltaFile])
         [[NSFileManager defaultManager] removeItemAtPath:signature error:nil];
        return deltaFile;
    return nil;
}

-(NSString *)createSignatureFromFile:(NSString *)file {
    NSString *signatureFile = [self createSignaturePath:file];
    if([_rsync signature:file sig:signatureFile])
        return signatureFile;
    return nil;
}

-(BOOL)patchFile:(NSString *)file withDelta:(NSString *)delta {
    NSString *patchedFile = [self createPatchedPath:file];
    if([_rsync patch:file delta:delta patch:patchedFile])
    {
        [[NSFileManager defaultManager] copyItemAtPath:patchedFile toPath:file error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:patchedFile error:nil];
        return YES;
    }
    return NO;
}

-(NSString *)createSignaturePath:(NSString *)file {
    NSString *signatureFile = [[file stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@".%@.sig",[file lastPathComponent]]];
    return signatureFile;
}

-(NSString *)createDeltaPath:(NSString *)file {
    NSString *deltaFile = [[file stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@".%@.delta",[file lastPathComponent]]];
    return deltaFile;
}

-(NSString *)createPatchedPath:(NSString *)file {
    NSString *patchedFile = [[file stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@".%@.patched",[file lastPathComponent]]];
    return patchedFile;
}

-(void)cleanup:(NSString *)file {
    [[NSFileManager defaultManager] removeItemAtPath:[self createSignaturePath:file] error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:[self createDeltaPath:file] error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:[self createPatchedPath:file] error:nil];
}

@end
