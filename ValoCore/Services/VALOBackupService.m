//
//  VALOBackupService.m
//  ValoCore
//
//  Created by Mike Fluff on 17.04.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOBackupService.h"
#import "VALOBackup.h"
#import <MagicalRecord/MagicalRecord.h>

@interface VALOBackupService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALOBackupService

- (instancetype)init {
    self = [super init];
    if (self) {
        //_localContext = [NSManagedObjectContext MR_context];
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    }
    return self;
}

- (instancetype)initWithDelegate:(id)delegate {
    self = [self init];
    if (!self) return nil;
    
    _delegate = delegate;
    
    
    return self;
}

-(NSArray *)getBackupPaths {
    NSArray *backups = [VALOBackup MR_findAll];
    return backups;
}

-(NSInteger)countBackupPaths {
    NSArray *backups = [VALOBackup MR_findAll];
    return backups.count;
}

-(void)refreshBackupPaths {
    NSError *error;
    BOOL isDirectory;
    NSArray  *BackupPaths = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:NSHomeDirectory() error:&error];
    for(NSString *path in BackupPaths) {
        if(![[path substringToIndex:1] isEqualToString:@"."]) {
            [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),path] isDirectory:&isDirectory];
            if(isDirectory) {
                if(![self checkPath:path]) {
                    VALOBackup *backupPath = [VALOBackup MR_createEntityInContext:_localContext];
                    backupPath.path = path;
                    backupPath.active = [NSNumber numberWithBool:NO];
                    backupPath.filesInside = [NSNumber numberWithInteger:[self countOfFilesInFolder:path]];
                }
                else {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"path = %@", path];
                    VALOBackup *backup = [VALOBackup MR_findFirstWithPredicate:predicate inContext:_localContext];
                    backup.filesInside = [NSNumber numberWithInteger:[self countOfFilesInFolder:path]];
                }
            }
        }
    }
    [_localContext MR_saveToPersistentStoreAndWait];
        
}

-(void)setActive:(VALOBackup *)backup status:(BOOL)status {
    backup = [backup MR_inContext:_localContext];
    backup.active = [NSNumber numberWithBool:status];
    [_localContext MR_saveToPersistentStoreAndWait];
}


-(NSInteger)countOfFilesInFolder:(NSString *)path {
    NSInteger count = 0;
    NSError *error;
    BOOL isDirectory;
    NSArray *contentOfPath = [[NSFileManager defaultManager]
                              subpathsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),path] error:&error];
    for(NSString *path in contentOfPath) {
        [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),path] isDirectory:&isDirectory];
        if(!isDirectory) {
            count++;
        }
    }
    return count;
}

-(BOOL)checkPath:(NSString *)path {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"path = %@", path];
    VALOBackup *backup = [VALOBackup MR_findFirstWithPredicate:predicate inContext:_localContext];
    if (backup) {
        return YES;
    }
    return NO;
}


@end
