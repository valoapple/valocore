//
//  VALOContactsService.m
//  ValoCore
//
//  Created by Kirill Shalankin on 14/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <FastEasyMapping/FastEasyMapping.h>
#import <MagicalRecord/MagicalRecord.h>

#import "VALOContactsService.h"
#import "VALOContact.h"
#import "VALONotificationCenterNameHolder.h"

@interface VALOContactsService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALOContactsService

- (instancetype)init {
    self = [super init];
    if (self) {
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    }
    return self;
}

- (void)saveContactsWithUsersJSON:(NSDictionary *)usersJSON {
    if (![self userInDatabase:usersJSON[@"id"]]) {
        FEMMapping *mapping = [VALOContact defaultMapping];
        
        [FEMDeserializer objectFromRepresentation:usersJSON
                                          mapping:mapping
                                          context:self.localContext];
        
        [self.localContext MR_saveToPersistentStoreAndWait];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[VALONotificationCenterNameHolder contactDidSave]
                                                            object:nil];
    }
}

#pragma mark - getters

- (NSArray *)allContactInDatabase {
    return [VALOContact MR_findAllSortedBy:@"lastName" ascending:YES];
}

#pragma mark - Private

- (BOOL)userInDatabase:(NSString *)userID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", userID];
    
    if ([VALOContact MR_findAllWithPredicate:predicate].count == 1) {
        return YES;
    }
    return NO;
}

@end
