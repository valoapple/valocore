//
//  VALONodeService.m
//  Valo
//
//  Created by Kirill Shalankin on 10/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <FastEasyMapping/FastEasyMapping.h>
#import <MagicalRecord/MagicalRecord.h>

#import "VALONodeService.h"
#import "VALONode.h"
#import "VALOVersion.h"
#import "VALONotificationCenterNameHolder.h"
#import "VALOUserDefaultsKeyHolder.h"
#import "VALOUserService.h"
#import "VALONodeTypeService.h"

@interface VALONodeService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALONodeService

- (instancetype)init {
    self = [super init];
    if (self) {
        //_localContext = [NSManagedObjectContext MR_context];
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    }
    return self;
}

- (instancetype)initWithDelegate:(id)delegate {
    self = [self init];
    if (!self) return nil;
    
    _delegate = delegate;
    
    
    return self;
}

#pragma mark - Save to database

- (void)saveNodeWithNodesJSON:(NSDictionary *)nodesJSON {
    if (![self nodeInDatabase:nodesJSON[@"id"] updatedAt:nodesJSON[@"updated_at"]]) {
        FEMMapping *mapping = [VALONode defaultMapping];
        
        [FEMDeserializer objectFromRepresentation:nodesJSON
                                          mapping:mapping
                                          context:self.localContext];
        
        [self setFileTypeWithNodeID:nodesJSON[@"id"]];
        [self bindParentWithNode:nodesJSON[@"id"]];
        
        [self.localContext MR_saveToPersistentStoreAndWait];
        
        [self databaseHasBeenUpdated];
    }
}

- (void)bindParentWithNode:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID inContext:_localContext];
    VALONode *parentNode = [VALONode MR_findFirstByAttribute:@"id" withValue:node.parentId inContext:_localContext];
    
    [parentNode addChildrenObject:node];
    [node setParent:parentNode];
    
    [self.localContext MR_saveToPersistentStoreAndWait];
}

- (void)setFileTypeWithNodeID:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID inContext:_localContext];
    
    if (node.isFile.boolValue) {
        node.type = [VALONodeTypeService nodeTypeWithName:node.name];
    }
    
    [self.localContext MR_saveToPersistentStoreAndWait];
}

#pragma getters

- (NSString *)mostRecentNodeDate
{

// Results should be in descending order of timeStamp.
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO];

    NSArray *results = [[VALONode MR_findAll] sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSString *dateString = [results objectAtIndex:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    dateFormatter2.dateFormat = @"yyyy-M-dd+HH:mm:ss+UTC";
    NSDate *date = [dateFormatter dateFromString:dateString];
    NSString *date2String = [dateFormatter2 stringFromDate:date];
    return date2String;
}


/*- (NSString *)pathForNode:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    if (node) {
        return node.path;
    }
    return nil;
}*/

- (BOOL)nodeInDatabase:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    if (node) {
        return YES;
    }
    return NO;
}

- (BOOL)nodeInDatabase:(NSString *)nodeID updatedAt:(NSString*)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
   
    NSDate *dateF = [dateFormatter dateFromString:date];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@ && updatedAt= %@", nodeID, dateF];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    if (node) {
        return YES;
    }
    return NO;
}


- (NSString *)nodeIDByNameAndParent:(NSString *)parentID nodeName:(NSString *)nodeName {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@ && name = %@", parentID, nodeName];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    NSString *nodeID = [node valueForKey:@"id"];
    return nodeID;
}

- (NSString *)parentIDForNode:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    NSString *parentID = [node valueForKey:@"parentId"];
    return parentID;
}

- (NSString *)versionIDForNode:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    VALOVersion *version = node.version;
    return [version valueForKey:@"id"];
}

-(NSString *)getNodePathByName:(NSString *)nodeName andNodeID:(NSString *)nodeID {
    NSString *path;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node;
    node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    path = [NSString stringWithFormat:@"%@",nodeName];
    while(node.parentId) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", node.parentId];
        node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
        path = [NSString stringWithFormat:@"%@/%@",node.name,path];
    }
    if([node.id isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:[VALOUserDefaultsKeyHolder rootNodeIdKey]]]) {
        VALOUserService *userService = [[VALOUserService alloc] init];
        NSString *userEmail = [userService getEmail];
        path = [NSString stringWithFormat:@"%@/%@",[NSString stringWithFormat:@"%@/%@/%@",NSHomeDirectory(),@"VALO-new",userEmail],path];
    }
    else if ([node.id isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:[VALOUserDefaultsKeyHolder backupNodeIdKey]]])
        path = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),path];
    return path;
}

-(NSString *)getNodePathById:(NSString *)nodeID {
    NSString *path;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node;
    node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    path = [NSString stringWithFormat:@"%@",node.name];
    while(node.parentId) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", node.parentId];
        node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
        path = [NSString stringWithFormat:@"%@/%@",node.name,path];
    }
    if([node.id isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:[VALOUserDefaultsKeyHolder rootNodeIdKey]]]) {
        VALOUserService *userService = [[VALOUserService alloc] init];
        NSString *userEmail = [userService getEmail];
        path = [NSString stringWithFormat:@"%@/%@",[NSString stringWithFormat:@"%@/%@/%@",NSHomeDirectory(),@"VALO-new",userEmail],path];
    }
    else if ([node.id isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:[VALOUserDefaultsKeyHolder backupNodeIdKey]]])
        path = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),path];
    return path;
}

- (NSDate *)findModifiedDatewithNodeID:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    return node.updatedAt;
}

-(NSInteger)countNodesWithParentID:(NSString *)parentID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@", parentID];
    NSArray *nodes = [VALONode MR_findAllWithPredicate:predicate inContext:_localContext];
    return nodes.count;
}

- (NSString *)findNodeIDwithSN:(NSNumber *)sn withSFN:(NSNumber *)sfn withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"systemnumber = %ld && systemfilenumber = %ld && parentsystemnumber = %ld && parentsystemfilenumber = %ld && deletedAt = nil", [sn integerValue], [sfn integerValue], [psn integerValue], [psfn integerValue]];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    return [node valueForKey:@"id"];
}

- (NSString *)findNodeIDbyName:(NSString *)name withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@ && parentsystemnumber = %ld && parentsystemfilenumber = %ld && deletedAt = nil", name, [psn integerValue], [psfn integerValue]];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    return [node valueForKey:@"id"];
}

- (NSString *)findNodeIDbyName:(NSString *)name {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    NSArray *nodes = [VALONode MR_findAllWithPredicate:predicate inContext:_localContext];
    if(nodes.count == 1)
    {
        VALONode *node = nodes[0];
        if(![node valueForKey:@"systemnumber"] && ![node valueForKey:@"systemfilenumber"])
            return node.id;
        else
            return nil;
    }
    else {
        return nil;
    }
}

- (BOOL)findNodebyID:(NSString *)nodeID withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@ && parentsystemnumber = %ld && parentsystemfilenumber = %ld && deletedAt = nil", nodeID, [psn integerValue], [psfn integerValue]];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    if(node)
        return YES;
    else
        return NO;
}

-(NSString *)findNodeWithSN:(NSNumber *)sn withSFN:(NSNumber *)sfn
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"systemnumber = %ld && systemfilenumber = %ld && deletedAt = nil", [sn integerValue], [sfn integerValue]];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    return node.id;
}

- (BOOL)nodeWithNodeID:(NSString *)nodeID hasSameName:(NSString *)name {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@ && name = %@", nodeID, name];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    if(node)
        return YES;
    else
        return NO;
}

-(NSArray *)namesArrayToDateFromParentID:(NSString *)parentID recentDate:(NSDate *)date {
    NSMutableArray *filesArray = [[NSMutableArray alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@ && deletedAt = nil", parentID];
    NSArray *nodes = [VALONode MR_findAllWithPredicate:predicate inContext:_localContext];
    for(VALONode *node in nodes) {
        if([[node valueForKey:@"modifiedAt"] compare:date] == NSOrderedDescending)
            [filesArray addObject:[node valueForKey:@"name"]];
    }
    return filesArray;
}

-(NSArray *)namesArrayToDateFromParentID:(NSString *)parentID {
    NSMutableArray *filesArray = [[NSMutableArray alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@ && deletedAt = nil", parentID];
    NSArray *nodes = [VALONode MR_findAllWithPredicate:predicate inContext:_localContext];
    for(VALONode *node in nodes) {
        NSLog(@"%@",[node MR_inContext:_localContext]);
        [filesArray addObject:[node valueForKey:@"name"]];
    }
    return filesArray;
}

-(NSString *)nodeNameByID:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    //NSLog(@"%@",[node valueForKey:@"name"]);
    return [node valueForKey:@"name"];
}

-(BOOL)nodeIsFile:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    //NSLog(@"%@",[node valueForKey:@"isFile"]);
    return [node.isFile boolValue];
}

-(BOOL)nodeInProgress:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    return [node.inProcess boolValue];
}

-(NSString *)inodePathForNode:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    NSString *path = [NSString stringWithFormat:@"/.vol/%@/%@", [[node valueForKey:@"systemnumber"] stringValue],[[node valueForKey:@"systemfilenumber"] stringValue]];
    return path;
}


#pragma setters

- (void)createNode:(NSString *)nodeID withName:(NSString *)name withParent:(NSString *)parentId withPath:(NSString *)path withDate:(NSString *)date isFile:(NSNumber *)isFile {
    VALONode *node = [VALONode MR_createEntityInContext:_localContext];
    [node setValue:nodeID forKey:@"id"];
    [node setValue:name forKey:@"name"];
    [node setValue:parentId forKey:@"parentId"];
    [node setValue:isFile forKey:@"isFile"];
    //[node setValue:path forKey:@"path"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *dateF = [dateFormatter dateFromString:date];
    [node setValue:dateF forKey:@"updatedAt"];
    VALONode *parentNode = [VALONode MR_findFirstByAttribute:@"id" withValue:node.parentId inContext:_localContext];
    
    if(parentNode)
        [parentNode addChildrenObject:node];
    
    [self updateNodeDatesWithDate:date nodeID:parentId];
    [self.localContext MR_saveToPersistentStoreAndWait];
    [self setFileTypeWithNodeID:nodeID];
}

- (void)createNode:(NSString *)nodeID withSN:(NSNumber *)sn withSFN:(NSNumber *)sfn withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn path:(NSString *)path {
    
    VALONode *node = [VALONode MR_createEntityInContext:_localContext];
    [node setValue:nodeID forKey:@"id"];
    [node setValue:[path lastPathComponent] forKey:@"name"];
    [node setValue:sn forKey:@"systemnumber"];
    [node setValue:sfn forKey:@"systemfilenumber"];
    [node setValue:psn forKey:@"parentsystemnumber"];
    [node setValue:psfn forKey:@"parentsystemfilenumber"];
    //[node setValue:path forKey:@"path"];
    [self.localContext MR_saveToPersistentStoreAndWait];
}

- (void)updateNodePSNPFSN:(NSString *)nodeId parentID:(NSString *)parentId {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeId];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"id = %@", parentId];
    VALONode *parent_node = [VALONode MR_findFirstWithPredicate:predicate2 inContext:_localContext];
    node.parentId = parentId;
    
    VALONode *parentNode = [VALONode MR_findFirstByAttribute:@"id" withValue:node.parentId inContext:_localContext];
    
    if(parentNode)
        [parentNode addChildrenObject:node];
    
    [node setValue:[parent_node valueForKey:@"systemnumber"] forKey:@"parentsystemnumber"];
    [node setValue:[parent_node valueForKey:@"systemfilenumber"] forKey:@"parentsystemfilenumber"];
    [self.localContext MR_saveToPersistentStoreAndWait];
}

- (void)setNode:(NSString *)nodeID withSN:(NSNumber *)sn withSFN:(NSNumber *)sfn withPSN:(NSNumber *)psn withPSFN:(NSNumber *)psfn path:(NSString *)path {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    [node setValue:sn forKey:@"systemnumber"];
    [node setValue:sfn forKey:@"systemfilenumber"];
    [node setValue:psn forKey:@"parentsystemnumber"];
    [node setValue:psfn forKey:@"parentsystemfilenumber"];
    //[node setValue:path forKey:@"path"];
    [_localContext MR_saveToPersistentStoreAndWait];
}

- (void)setNode:(NSString *)nodeID withModifiedDate:(NSDate *)date {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    [node setValue:date forKey:@"modifiedAt"];
    [self.localContext MR_saveToPersistentStoreAndWait];
}

- (void)setNode:(NSString *)nodeID progress:(BOOL)progress {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    node.inProcess = [NSNumber numberWithBool:progress];
    [self.localContext MR_saveToPersistentStoreAndWait];
}

- (void)setNode:(NSString *)nodeID isFile:(BOOL)isFile {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate inContext:_localContext];
    node.isFile = [NSNumber numberWithBool:isFile];
    [self.localContext MR_saveToPersistentStoreAndWait];
}

#pragma mark - Delete nodes

- (void)markDeletedNodeWithNodeID:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID inContext:_localContext];
    
    NSDate *currentDate = [[NSDate alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSString *localDateString = [dateFormatter stringFromDate:currentDate];
    
    node.deletedAt = [dateFormatter dateFromString:localDateString];
     NSLog(@"%@",[node MR_inContext:_localContext]);
    [_localContext MR_saveToPersistentStoreAndWait];
}

- (void)deleteNodeId:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID inContext:_localContext];
    [node MR_deleteEntityInContext:_localContext];
    [_localContext MR_saveToPersistentStoreAndWait];
    
}

#pragma mark - Update node 

- (void)updateNodeNameWithNodeName:(NSString *)name nodeID:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID inContext:_localContext];
    
    node.name = [name lastPathComponent];
    //node.path = name;
    [self.localContext MR_saveToPersistentStoreAndWait];
}

- (void)updateNodeDatesWithDate:(NSString *)date nodeID:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID inContext:_localContext];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    node.updatedAt = [dateFormatter dateFromString:date];
    if(node.parentId)
        [self updateNodeDatesWithDate:date nodeID:node.parentId];
    [self.localContext MR_saveToPersistentStoreAndWait];
}


- (void)markNodeAsShared:(NSString *)nodeID {
    VALONode *node = [VALONode MR_findFirstByAttribute:@"id" withValue:nodeID];
    
    node.shared = [NSNumber numberWithBool:YES];
    
    [self.localContext MR_saveToPersistentStoreAndWait];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[VALONotificationCenterNameHolder nodeWasUpdatedKey] object:nil];
    
}

#pragma mark - getters

- (NSArray *)allNodeInDatabase {
    return [VALONode MR_findAllSortedBy:@"isFile:YES,name" ascending:YES];
}

- (NSArray *)childrenNodesForNodeID:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@ && deletedAt = nil", nodeID];
    
    NSSortDescriptor *fileDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"isFile" ascending:YES];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[fileDescriptor, nameDescriptor];
    
    return [[VALONode MR_findAllWithPredicate:predicate] sortedArrayUsingDescriptors:sortDescriptors];
}

- (NSArray *)recyclerNodesForNodeID:(NSString *)nodeID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@ && deletedAt != nil", nodeID];
    
    NSSortDescriptor *fileDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"isFile" ascending:YES];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[fileDescriptor, nameDescriptor];
    
    return [[VALONode MR_findAllWithPredicate:predicate] sortedArrayUsingDescriptors:sortDescriptors];
}

- (NSArray *)nodeNamesWithParentID:(NSString *)parentID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentId = %@ && deletedAt = nil", parentID];
    
    NSSortDescriptor *fileDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"isFile" ascending:YES];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[fileDescriptor, nameDescriptor];
    
    return [[VALONode MR_findAllWithPredicate:predicate] sortedArrayUsingDescriptors:sortDescriptors];
}

#pragma mark - Notifications

- (void)databaseHasBeenUpdated {

#if TARGET_OS_IOS
        [[NSNotificationCenter defaultCenter] postNotificationName:[VALONotificationCenterNameHolder updateDatabaseKey] object:nil];
#endif
#if TARGET_OS_MAC
    NSString *observedObject = @"com.iqreserve.Valo";
    NSDistributedNotificationCenter *center =
    [NSDistributedNotificationCenter defaultCenter];
    [center postNotificationName: [VALONotificationCenterNameHolder updateDatabaseKey]
                          object: observedObject
                        userInfo: [[NSDictionary alloc] initWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",nil] deliverImmediately: YES];
    
    
    if(_delegate)
        [_delegate performSelectorOnMainThread:@selector(nodesUpdated) withObject:nil waitUntilDone:NO];
#endif
}

#pragma mark - Helpers 

- (NSString *)getUTCFormateDate:(NSDate *)currentDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];

    return [dateFormatter stringFromDate:currentDate];
}

@end
