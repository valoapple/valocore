//
//  VALONodeImageService.m
//  Valo
//
//  Created by Kirill Shalankin on 08/04/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//


#import "VALONodeImageServiceOSX.h"

@implementation VALONodeImageService

+ (NSImage *)imageWithNodeType:(fileType)type {
    if (type == kVALOVideoFileType) {
        return [NSImage imageNamed:@"Video_icon"];
        
    } else if (type == kVALOAudioFileType) {
        return [NSImage imageNamed:@"Audio_icon"];
        
    } else if (type == kVALODocumentFileType) {
        return [NSImage imageNamed:@"DOC_icon"];
        
    } else if (type == kVALOSpreadsheetFileType) {
        return [NSImage imageNamed:@"XLS_icon"];
        
    } else if (type == kVALOPresentationFileType) {
        return [NSImage imageNamed:@"PPT_icon"];
        
    } else if (type == kVALOPDFFileType) {
        return [NSImage imageNamed:@"PDF_icon"];
        
    } else if (type == kVALOTextFileType) {
        return [NSImage imageNamed:@"TXT_icon"];
        
    } else if (type == kVALOContactCardFileType) {
        return [NSImage imageNamed:@"File_icon"];
        
    } else if (type == kVALOImageFileType) {
        return [NSImage imageNamed:@"IMG_icon"];
        
    } else {
        return [NSImage imageNamed:@"File_icon"];
    }
}

+ (NSImage *)largeImageWithNodeType:(fileType)type {
    if (type == kVALOVideoFileType) {
        return [NSImage imageNamed:@"Video_icon_large"];
        
    } else if (type == kVALOAudioFileType) {
        return [NSImage imageNamed:@"Audio_icon_large"];
        
    } else if (type == kVALODocumentFileType) {
        return [NSImage imageNamed:@"DOC_icon_large"];
        
    } else if (type == kVALOSpreadsheetFileType) {
        return [NSImage imageNamed:@"XLS_icon_large"];
        
    } else if (type == kVALOPresentationFileType) {
        return [NSImage imageNamed:@"PPT_icon_large"];
        
    } else if (type == kVALOPDFFileType) {
        return [NSImage imageNamed:@"PDF_icon_large"];
        
    } else if (type == kVALOTextFileType) {
        return [NSImage imageNamed:@"TXT_icon_large"];
        
    } else if (type == kVALOContactCardFileType) {
        return [NSImage imageNamed:@"File_icon_large"];
        
    } else if (type == kVALOImageFileType) {
        return [NSImage imageNamed:@"Image_icon_large"];
        
    } else {
        return [NSImage imageNamed:@"File_icon_large"];
    }
}

@end
