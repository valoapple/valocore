//
//  VALODeviceService.h
//  Valo
//
//  Created by Kirill Shalankin on 11/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALODeviceService : NSObject

- (void)saveDeviceWithUserJSON:(NSDictionary *)deviceJSON;
- (NSArray *)allDevicesInDatabase;

- (NSString *)selfDeviceId;

@end
