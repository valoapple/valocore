//
//  VALOUserService.h
//  Valo
//
//  Created by Kirill Shalankin on 11/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALOUserService : NSObject

- (void)saveUserWithUserJSON:(NSDictionary *)userJSON;

- (NSString *)getFirstName;
- (NSString *)getLastName;
- (NSString *)getEmail;
- (NSString *)getUserID;

@end
