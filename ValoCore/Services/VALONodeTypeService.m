//
//  VALONodeTypeService.m
//  ValoCore
//
//  Created by Kirill Shalankin on 20/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALONodeTypeService.h"
#import "VALONode.h"

@implementation VALONodeTypeService

+ (NSInteger)nodeTypeWithName:(NSString *)text {
    NSArray *fileExtension = @[[VALONodeTypeService videoNamesArray],
                               [VALONodeTypeService audioNamesArray],
                               [VALONodeTypeService imageNamesArray],
                               [VALONodeTypeService documentNamesArray],
                               [VALONodeTypeService spreadsheetNamesArray],
                               [VALONodeTypeService presentationNamesArray],
                               [VALONodeTypeService PDFNamesArray],
                               [VALONodeTypeService textNamesArray],
                               [VALONodeTypeService contactNamesArray]];
    
    for (NSArray *namesArray in fileExtension) {
        for (NSString *extension in namesArray) {
            if ([[text uppercaseString] containsString:extension]) {
                if (namesArray == [VALONodeTypeService videoNamesArray]) {
                    return kVALOVideoFileType;
                } else if (namesArray == [VALONodeTypeService audioNamesArray]) {
                    return kVALOAudioFileType;
                } else if (namesArray == [VALONodeTypeService documentNamesArray]) {
                    return kVALODocumentFileType;
                } else if (namesArray == [VALONodeTypeService spreadsheetNamesArray]) {
                    return kVALOSpreadsheetFileType;
                } else if (namesArray == [VALONodeTypeService presentationNamesArray]) {
                    return kVALOPresentationFileType;
                } else if (namesArray == [VALONodeTypeService PDFNamesArray]) {
                    return kVALOPDFFileType;
                } else if (namesArray == [VALONodeTypeService textNamesArray]) {
                    return kVALOTextFileType;
                } else if (namesArray == [VALONodeTypeService contactNamesArray]) {
                    return kVALOContactCardFileType;
                } else if (namesArray == [VALONodeTypeService imageNamesArray]) {
                    return kVALOImageFileType;
                }
            }
        }
    }
    
    return kVALODefaultFileType;
}

+ (NSArray *)videoNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".WEBM", @".MKV", @".FLV", @".OGV", @".AVI", @".MOV", @".WMV", @".ASF", @".MP4", @".M4V", @".MPG",
                    @".MPEG", @".M2V"];
    });
    return _titles;
}

+ (NSArray *)audioNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".AAC", @".FLAC", @".M4A", @".MP3", @".OGG", @".OGA", @".WAV", @".WMA"];
    });
    return _titles;
}

+ (NSArray *)imageNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".JPG", @".JPEG", @".PNG", @".GIF", @".TIFF", @".BMP", @".SVG"];
    });
    return _titles;
}

+ (NSArray *)documentNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".DOC", @".DOCX", @".DOCM", @".DOTX", @".DOTM", @".ODT", @".OTT", @".OTH", @".ODM"];
    });
    return _titles;
}

+ (NSArray *)spreadsheetNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".XLS", @".XLT", @".XLW", @".XLSM", @".XLTM", @".XLTX", @".ODS", @".OTS", @".CSV"];
    });
    return _titles;
}

+ (NSArray *)presentationNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".PPT", @".PPTX"];
    });
    return _titles;
}

+ (NSArray *)PDFNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".PDF"];
    });
    return _titles;
}

+ (NSArray *)textNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".TXT"];
    });
    return _titles;
}

+ (NSArray *)contactNamesArray {
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@".VCF", @".VCARD"];
    });
    return _titles;
}

@end
