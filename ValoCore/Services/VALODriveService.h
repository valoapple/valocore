//
//  VALODriveService.h
//  Valo
//
//  Created by Kirill Shalankin on 11/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALODriveService : NSObject

- (void)saveDriveWithUserJSON:(NSDictionary *)driveJSON;
- (NSArray *)getDrivesForDeviceID:(NSString *)deviceID;

@end
