//
//  VALOContactsService.h
//  ValoCore
//
//  Created by Kirill Shalankin on 14/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALOContactsService : NSObject

- (void)saveContactsWithUsersJSON:(NSDictionary *)usersJSON;
- (NSArray *)allContactInDatabase;

@end
