//
//  VALOVersionService.m
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <FastEasyMapping/FastEasyMapping.h>
#import <MagicalRecord/MagicalRecord.h>


#import "VALOVersionService.h"
#import "VALOVersion.h"
#import "VALONode.h"

@interface VALOVersionService ()

@property (strong, nonatomic) NSManagedObjectContext *localContext;

@end

@implementation VALOVersionService

- (instancetype)init {
    self = [super init];
    if (self) {
        _localContext = [NSManagedObjectContext MR_newPrivateQueueContext];
        [_localContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    }
    return self;
}


- (void)saveVersionWithVersionJSON:(NSDictionary *)versionJSON {
    if (![self versionInDatabase:versionJSON[@"id"]]) {
        FEMMapping *mapping = [VALOVersion defaultMapping];
        
        [FEMDeserializer objectFromRepresentation:versionJSON
                                          mapping:mapping
                                          context:self.localContext];
        
        [self.localContext MR_saveToPersistentStoreAndWait];
    }
}

- (void)linkVersion:(NSString *)versionID withNode:(NSString *)nodeID
{
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id = %@", versionID];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"id = %@", nodeID];
    VALOVersion *version = [VALOVersion MR_findFirstWithPredicate:predicate1 inContext:_localContext];
    VALONode *node = [VALONode MR_findFirstWithPredicate:predicate2 inContext:_localContext];
    if(version && node) {
        version.node = node;
        [self.localContext MR_saveToPersistentStoreAndWait];
    }
    
}

- (BOOL)checkVersion:(NSString *)versionID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", versionID];
    VALOVersion *version = [VALOVersion MR_findFirstWithPredicate:predicate inContext:_localContext];
    if(version)
        return YES;
    else
        return NO;
}

- (BOOL)versionIsOlder:(NSDictionary *)version {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", [version valueForKey:@"id"]];
    VALOVersion *ver = [VALOVersion MR_findFirstWithPredicate:predicate inContext:_localContext];
    if([[ver valueForKey:@"checksum"] isEqualToString:[version valueForKey:@"checksum"]])
        return NO;
    else
        return YES;
}
- (NSString *)hashForVerison:(NSString *)versionID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", versionID];
    VALOVersion *version = [VALOVersion MR_findFirstWithPredicate:predicate inContext:_localContext];
    return version.checksum;
}

#pragma mark - Private

- (BOOL)versionInDatabase:(NSString *)versionID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", versionID];
    
    if ([VALOVersion MR_findFirstWithPredicate:predicate inContext:_localContext]) {
        return YES;
    }
    return NO;
}


@end
