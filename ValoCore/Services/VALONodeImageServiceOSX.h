//
//  VALONodeImageService.h
//  Valo
//
//  Created by Kirill Shalankin on 08/04/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "VALONode.h"

@interface VALONodeImageService : NSObject

+ (NSImage *)imageWithNodeType:(fileType)type;
+ (NSImage *)largeImageWithNodeType:(fileType)type;


@end
