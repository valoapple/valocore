//
//  VALOActivitiesService.m
//  ValoCore
//
//  Created by Mike Fluff on 27.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOActivitiesService.h"
#import "VALOFileService.h"
#import "VALONodeService.h"
#import "VALOGetNodeByIDOperation.h"
#import "VALOUserDefaultsKeyHolder.h"

@implementation VALOActivitiesService

- (instancetype)init {
    self = [super init];
    if (self) {
        _nodeService = [[VALONodeService alloc] init];
        _fileService = [[VALOFileService alloc] init];
        _accessToken = [[NSUserDefaults standardUserDefaults]objectForKey: [VALOUserDefaultsKeyHolder accessTokenKey]];
    }
    return self;
}

-(BOOL)confirmCreateItem:(NSDictionary *)activity {
    return ![_nodeService nodeInDatabase:[activity valueForKey:@"node_id"]];
}

-(NSArray *)rebuildActivity:(NSArray *)activities {
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:NO];
    activities = [activities sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    NSMutableArray *rebuildedActivities = [[NSMutableArray alloc] init];
    NSArray *nodesArray = [activities valueForKeyPath:@"@distinctUnionOfObjects.node_id"];
    for(NSString *nodeID in nodesArray) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"node_id = %@",nodeID];
        //get all activities for node id
        NSArray *temparr = [activities filteredArrayUsingPredicate:predicate];
        //check if there is only one activity
        
        if(temparr.count == 1)
        {
            if(![[temparr[0] valueForKey:@"is_file"] integerValue])
                [rebuildedActivities insertObject:temparr[0] atIndex:0];
            else
                [rebuildedActivities addObject:temparr[0]];
        }
        else {
            //check if item on top is delete
            if([[temparr[0] valueForKey:@"action"] isEqualToString:@"delete_item"])
            {
                [rebuildedActivities addObject:temparr[0]];
            }
            else {
                //creating temp filtred activities array for node
                NSMutableArray *nodeActivitiesArray = [[NSMutableArray alloc] init];
                BOOL version = YES;
                BOOL action = YES;
                BOOL isDir = NO;
                for(NSDictionary *activity in temparr)
                {
                    if([[activity valueForKey:@"action"] isEqualToString:@"create_item"])
                        if(action)
                        {
                            [nodeActivitiesArray insertObject:activity atIndex:0];
                            action = NO;
                            if(![[activity valueForKey:@"is_file"] integerValue])
                                isDir = YES;
                        }
                    if([[activity valueForKey:@"action"] isEqualToString:@"add_version"])
                    {
                        if(version)
                        {
                            [nodeActivitiesArray addObject:activity];
                            version = NO;
                        }
                    }
                    if([[activity valueForKey:@"action"] isEqualToString:@"rename_item"])
                    {
                        if(action)
                        {
                            [nodeActivitiesArray insertObject:activity atIndex:0];
                            action = NO;
                            if(![[activity valueForKey:@"is_file"] integerValue])
                                isDir = YES;
                            
                        }
                    }
                    if([[activity valueForKey:@"action"] isEqualToString:@"move_item"])
                    {
                        if(action)
                        {
                            [nodeActivitiesArray insertObject:activity atIndex:0];
                            action = NO;
                            if(![[activity valueForKey:@"is_file"] integerValue])
                                isDir = YES;
                        }
                    }
                    if(!version && !action)
                    {
                        break;
                    }
                        
                }
                if(isDir) {
                    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                                           NSMakeRange(0,[nodeActivitiesArray count])];
                    [rebuildedActivities insertObjects:nodeActivitiesArray atIndexes:indexes];
                    
                }
                else
                    [rebuildedActivities addObjectsFromArray:nodeActivitiesArray];
            }

        }
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_file = 0"];
    //get all activities for node id
    NSArray *temparr = [rebuildedActivities filteredArrayUsingPredicate:predicate];
    descriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:YES];
    temparr = [temparr sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(0,[temparr count])];
    [rebuildedActivities replaceObjectsAtIndexes:indexes withObjects:temparr];
    //if([[NSUserDefaults standardUserDefaults]objectForKey: [VALOUserDefaultsKeyHolder lastActivityDate]])
    //    [rebuildedActivities removeObjectAtIndex:0];
    
    /*descriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:YES];
    NSArray *rebuildedActivitiesSorted;
    rebuildedActivitiesSorted = [rebuildedActivities sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];*/
    //NSArray* reversedArray = [[rebuildedActivities reverseObjectEnumerator] allObjects];
  
    return rebuildedActivities;
}


@end
