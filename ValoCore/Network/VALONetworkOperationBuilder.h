//
//  VALONetworkOperationBuilder.h
//  Valo
//
//  Created by Kirill Shalankin on 12.01.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking.h>

typedef NS_ENUM(NSInteger, VALONetworkOperationMethod) {
    VALONetworkOperationGETMethod,
    VALONetworkOperationPOSTMethod,
    VALONetworkOperationPOSTFORMMethod,
    VALONetworkOperationDELETEMethod,
    VALONetworkOperationPatchMethod
};

typedef void (^form)(id <AFMultipartFormData> formData);

@interface VALONetworkOperationBuilder : NSObject

- (instancetype)initWithPath:(NSString *)path;

@property (nonatomic, strong) AFHTTPSessionManager *manager;

@property (nonatomic, assign) VALONetworkOperationMethod method;

@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong) NSMutableDictionary *parameters;

@property (nonatomic,strong) form form;

- (NSURL *)baseURL;

@end
