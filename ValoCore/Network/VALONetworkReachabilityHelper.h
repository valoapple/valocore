//
//  VALONetworkReachabilityHelper.h
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALONetworkReachabilityHelper : NSObject

+ (BOOL)isNetworkReachable;

@end
