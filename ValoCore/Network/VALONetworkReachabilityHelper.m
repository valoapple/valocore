//
//  VALONetworkReachabilityHelper.m
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <AFNetworking.h>

#import "VALONetworkReachabilityHelper.h"

@implementation VALONetworkReachabilityHelper

+ (BOOL)isNetworkReachable {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

@end
