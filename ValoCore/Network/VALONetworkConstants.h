//
//  VLNetworkConstants.h
//  Valo
//
//  Created by Kirill Shalankin on 13/01/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kVALOAccessTokenKey;
extern NSString *const kVALOAgentKey;

extern NSString *const kVALOAcceptKey;
extern NSString *const kVALOAcceptType;

extern NSInteger const kVALODataSplitSize;
extern NSInteger const kVALOUploadMaxCount;
extern NSInteger const kVALODefaultUploadSpeed;
