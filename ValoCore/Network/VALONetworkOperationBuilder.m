//
//  VALONetworkOperationBuilder.m
//  Valo
//
//  Created by Kirill Shalankin on 12.01.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALONetworkOperationBuilder.h"
#import "VALONetworkURLManager.h"
#import "VALOUserDefaultsKeyHolder.h"

@interface VALONetworkOperationBuilder ()

@property (nonatomic, copy) NSString *baseURLString;

@end

@implementation VALONetworkOperationBuilder

- (instancetype)initWithPath:(NSString *)path {
    self = [super init];
    if (self) {
        NSUserDefaults *userDefaults = [[NSUserDefaults alloc] initWithSuiteName:
                                        [VALOUserDefaultsKeyHolder suitNameKey]];
        
        if ([userDefaults objectForKey:[VALOUserDefaultsKeyHolder savedHostKey]]) {
            
            if ([userDefaults objectForKey:[VALOUserDefaultsKeyHolder configDictionaryKey]]) {
                NSDictionary *config = [userDefaults objectForKey:[VALOUserDefaultsKeyHolder configDictionaryKey]];
                _baseURLString = config[@"backendURL"];
            } else {
                _baseURLString = [userDefaults objectForKey:[VALOUserDefaultsKeyHolder savedHostKey]];
            }
            
        } else {
            _baseURLString = [VALONetworkURLManager baseURLString];
        }
        _path = [NSString stringWithFormat:@"%@%@%@", self.baseURLString, [VALONetworkURLManager backendURLString], path];
        
        if ([path isEqualToString:[VALONetworkURLManager configPathString]]) {
            _path = [NSString stringWithFormat:@"%@%@", self.baseURLString, path];
        }
    }
    return self;
}

- (NSURL *)baseURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.baseURLString]];
}

@end
