//
//  VALONetworkURLManager.h
//  Valo
//
//  Created by Kirill Shalankin on 12.01.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALONetworkURLManager : NSObject

+ (NSString *)configPathString;

+ (NSString *)baseURLString;
+ (NSString *)backendURLString;

+ (NSString *)uploadPathString;
+ (NSString *)loginPathString;
+ (NSString *)nodePathString;
+ (NSString *)oneNodePathString;
+ (NSString *)drivePathString;
+ (NSString *)rootNodePathString;
+ (NSString *)registerDevicePathString;
+ (NSString *)bindDevicePathString;
+ (NSString *)usersPathString;
+ (NSString *)devicesPathString;
+ (NSString *)nodeChildrenPathString;
+ (NSString *)blockPathString;
+ (NSString *)downloadPathString;
+ (NSString *)shareNodePathString;
+ (NSString *)activitiesPathString;

@end
