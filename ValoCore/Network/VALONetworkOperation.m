//
//  VALONetworkOperation.m
//  Valo
//
//  Created by Kirill Shalankin on 13/01/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALONetworkOperation.h"
#import "VALONetworkOperationBuilder.h"

#import <AFNetworking.h>

@interface VALONetworkOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *builder;

@end

@implementation VALONetworkOperation

- (instancetype)initWithBuilder:(VALONetworkOperationBuilder *)builder {
    self = [super init];
    if (self) {
        _builder = builder;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulNetworkOperation)success failure:(FailedNetworkOperation)failure {
   
    switch (self.builder.method) {
        case VALONetworkOperationGETMethod: {
            [self.builder.manager GET:self.builder.path  parameters:[self.builder parameters] progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
            break;
            
        } case VALONetworkOperationPOSTMethod: {
            [self.builder.manager POST:self.builder.path parameters:[self.builder parameters] progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
            break;
            
        }
        case VALONetworkOperationPOSTFORMMethod: {
            [self.builder.manager POST:self.builder.path parameters:[self.builder parameters] constructingBodyWithBlock:self.builder.form progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                success(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
             
            break;
            
        } case VALONetworkOperationDELETEMethod: {
            [self.builder.manager DELETE:self.builder.path parameters:self.builder.parameters success:^(NSURLSessionDataTask * _Nonnull task,
                                                                             id  _Nullable responseObject) {
                success(responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
            break;
          
        } case VALONetworkOperationPatchMethod: {
            [self.builder.manager PATCH:self.builder.path parameters:self.builder.parameters success:^(NSURLSessionDataTask * _Nonnull task,
                                                                                                        id  _Nullable responseObject) {
                success(responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
            break;
            
        } default:
            NSAssert(nil, @"Choose VALONetworkOperationMethod!");
            break;
    }
}

- (void)failureHandlerWithTask:(NSURLSessionDataTask * _Nullable)task error:(NSError * _Nonnull)error {
//    NSNumber *errorCode = errorMessage.userInfo[@"code"];
//    if (failure) {
//        NSString *message;
//        if ([NetworkReachabilityHelper isNetworkReachable]) {
//            if ([errorMessage.errorMessage length]) {
//                message = errorMessage.errorMessage;
//            } else {
//                message = NSLocalizedString(@"Network.errorOccuredTryAgainLaterMessage", @"Network error message");
//            }
//        } else {
//            message = NSLocalizedString(@"Network.internetNecessaryMessage", @"No internet error message");
//            errorCode = @(kNoInternetConnectionErrorCode);
//        }
//        failure([errorCode integerValue], message);
//    }
//    
//    NSInteger statusCode = operation.HTTPRequestOperation.response.statusCode;
//    if (statusCode == 403) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kAccessDeniedNotification object:errorMessage.errorMessage];
//    }
}

@end
