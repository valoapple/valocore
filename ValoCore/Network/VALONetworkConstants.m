//
//  VLNetworkConstants.m
//  Valo
//
//  Created by Kirill Shalankin on 13/01/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALONetworkConstants.h"

NSString *const kVALOAccessTokenKey   = @"X-Nexetic-Auth";
NSString *const kVALOAgentKey         = @"User-Agent";

NSString *const kVALOAcceptType       = @"Accept";
NSString *const kVALOAcceptKey        = @"application/valo.api.v2";

NSInteger const kVALODataSplitSize        = 4096;
NSInteger const kVALOUploadMaxCount       = 1024;
NSInteger const kVALODefaultUploadSpeed   = 500;