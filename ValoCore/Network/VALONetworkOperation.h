//
//  VALONetworkOperation.h
//  Valo
//
//  Created by Kirill Shalankin on 13/01/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulNetworkOperation)(id response);
typedef void(^FailedNetworkOperation)(NSError *error);

@class VALONetworkOperationBuilder;

@interface VALONetworkOperation : NSObject

- (instancetype)initWithBuilder:(VALONetworkOperationBuilder *)builder;
- (void)addOperationWithSuccess:(SuccessfulNetworkOperation)success failure:(FailedNetworkOperation)failure;

@end
