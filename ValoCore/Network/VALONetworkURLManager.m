//
//  VALONetworkURLManager.m
//  Valo
//
//  Created by Kirill Shalankin on 12.01.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALONetworkURLManager.h"

@implementation VALONetworkURLManager

static NSString *const kVALODefaultBaseURL = @"http://demo.valo.pro";

// URL type: http://demo.valo.pro/backend
static NSString *const kVALOBackendURL = @"/api";

// Full path type:http://demo.valo.pro/nodes/api/nodes
static NSString *const kVALONodePath                         = @"/nodes";
static NSString *const kVALOOneNodePath                      = @"/nodes/%@";
static NSString *const kVALODrivePath                        = @"/drives";
static NSString *const kVALODevicesPath                      = @"/devices";
static NSString *const kVALOUsersPath                        = @"/users";
static NSString *const kVALOActionsPath                      = @"/actions";
static NSString *const kVALOActivitiesPath                   = @"/activities";
static NSString *const kVALOCheckPath                        = @"/check";
static NSString *const kVALOUploadPath                       = @"/uploads";
static NSString *const kVALOBlockPath                        = @"/blocks";
static NSString *const kVALOCreateTransactionsPath           = @"/create_transaction";
static NSString *const kVALOUserForSharingPath               = @"/users_for_sharing";
static NSString *const kVALOShareNodePath                    = @"/share_node";
static NSString *const kVALODownloadExtensionBundlesPath     = @"/extensions_bundles";
static NSString *const kVALODownloadSystemFilesPath          = @"/system_files";
static NSString *const kVALOConfigPath                       = @"/config.json";

static NSString *const kVALOLoginWithEmailPath   = @"/auth/login";
static NSString *const kVALOBindDevicePath       = @"/auth/bind_device";
static NSString *const kVALORegisterDevicePath   = @"/auth/register_device";
static NSString *const kVALOLogoutPath           = @"/auth/logout";
static NSString *const kVALOResetPasswordPath    = @"/auth/reset_password";
static NSString *const kVALOUpdatePasswordPath   = @"/auth/update_password";
static NSString *const kVALOCheckPingPath        = @"/auth/ping";

static NSString *const kVALOProfileUpdatePath   = @"/profile/update";
static NSString *const kVALOUpdateAvatarPath    = @"/profile/update_avatar";

static NSString *const kVALOSetVersionPath          = @"/nodes/%@/versions/%@/set_as_current";
static NSString *const kVALOHardDeleteNodesPath     = @"/nodes/hard_delete";
static NSString *const kVALOMoveFilePath            = @"/nodes/%@/move";
static NSString *const kVALOShareDrivesPath         = @"/nodes/%@/share_drives";
static NSString *const kVALORootNodePath            = @"/nodes/nodes/%@/tree";
static NSString *const kVALONodeChildren            = @"/children";

static NSString *const kVALODownloadFilePath    = @"/files/%@/%@";
static NSString *const kVALOBuildFilePath       = @"/versions/%@/build_file";

static NSString *const kVALOUploadBackupStatusPath = @"/devices/%@/set_backup_status";

static NSString *const kVALOUploadPrefencesPath = @"/preferences/%@/update";


+ (NSString *)configPathString {
    return kVALOConfigPath;
}

+ (NSString *)baseURLString {
    return kVALODefaultBaseURL;
}

+ (NSString *)backendURLString {
    return kVALOBackendURL;
}

+ (NSString *)uploadPathString {
    return kVALOUploadPath;
}

+ (NSString *)loginPathString {
    return kVALOLoginWithEmailPath;
}

+ (NSString *)nodePathString {
    return kVALONodePath;
}

+ (NSString *)oneNodePathString {
    return kVALOOneNodePath;
}

+ (NSString *)drivePathString {
    return kVALODrivePath;
}

+ (NSString *)rootNodePathString {
    return kVALORootNodePath;
}

+ (NSString *)registerDevicePathString {
    return kVALORegisterDevicePath;
}

+ (NSString *)bindDevicePathString {
    return kVALOBindDevicePath;
}

+ (NSString *)usersPathString {
    return kVALOUsersPath;
}

+ (NSString *)devicesPathString {
    return kVALODevicesPath;
}

+ (NSString *)nodeChildrenPathString {
    return kVALONodeChildren;
}

+ (NSString *)blockPathString {
    return kVALOBlockPath;
}

+ (NSString *)downloadPathString {
    return kVALODownloadFilePath;
}

+ (NSString *)shareNodePathString {
    return kVALOShareNodePath;
}

+ (NSString *)activitiesPathString {
    return kVALOActivitiesPath;
}

@end
