//
//  VALOGetUsersOperation.h
//  ValoCore
//
//  Created by Kirill Shalankin on 11/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetUsersNetworkOperation)(NSDictionary *users);
typedef void(^FailedGetUsersNetworkOperation)(NSError *error);

@interface VALOGetUsersOperation : NSObject

- (instancetype)initWithUserIDs:(NSArray *)userIDs token:(NSString *)token;

- (void)addOperationWithSuccess:(SuccessfulGetUsersNetworkOperation)success
                        failure:(FailedGetUsersNetworkOperation)failure;

@end
