//
//  VALOGetNodeByIDOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 20.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetNodeByIDOperation)(id response);
typedef void(^FailedGetNodeByIDOperation)(NSError *error);


@interface VALOGetNodeByIDOperation : NSObject

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulGetNodeByIDOperation)success
                        failure:(FailedGetNodeByIDOperation)failure;

@end
