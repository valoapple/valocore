//
//  VALOGetConfigOperation.m
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <AFNetworking.h>

#import "VALOGetConfigOperation.h"
#import "VALONetworkOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkURLManager.h"
#import "VALOUserDefaultsKeyHolder.h"
#import "VALONetworkConstants.h"

@interface VALOGetConfigOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOGetConfigOperation

- (instancetype)init {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",[VALONetworkURLManager configPathString]]];

        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];

        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetConfigNetworkOperation)success
                            failure:(FailedGetConfigNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];

    [self.operation addOperationWithSuccess:^(id response) {
        NSString *backendURLString  = response[@"BACKEND_ENDPOINT"];
        NSString *fayeURLString     = response[@"NODE_NOTIFICATIONS_ENDPOINT"];
        NSString *helpURLString     = response[@"HELP_LINK"];
        NSString *adminMailString   = response[@"MAIL_ADMIN_LINK"];
        
        NSDictionary *config = @{@"backendURL"  : backendURLString,
                                 @"fayeURL"     : fayeURLString};
        
        NSDictionary *helpConfig = @{@"helpURL"     : helpURLString,
                                     @"adminMail"   : adminMailString};
        
        NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:[VALOUserDefaultsKeyHolder suitNameKey]];
        [defaults setObject:config forKey:[VALOUserDefaultsKeyHolder configDictionaryKey]];
        [defaults setObject:helpConfig forKey:[VALOUserDefaultsKeyHolder helpDictionaryKey]];
        
        success(true);
        
    } failure:^(NSError *error) {
        NSLog(@"FailedGetConfigNetworkOperation: %@", error);
        failure(error);
    }];
}

@end
