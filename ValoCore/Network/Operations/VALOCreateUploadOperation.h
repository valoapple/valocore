//
//  VALOCreateUploadOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 15.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulCreateUploadOperation)(NSDictionary *result);
typedef void(^FailedCreateUploadOperation)(NSError *error);

@interface VALOCreateUploadOperation : NSObject

- (instancetype)initWithNodeID:(NSString *)nodeID versionID:(NSString *)versionID fileSize:(NSString *)fileSize type:(NSString *)type token:(NSString *)token;

- (void)addOperationWithSuccess:(SuccessfulCreateUploadOperation)success
                        failure:(FailedCreateUploadOperation)failure;

@end
