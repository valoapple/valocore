//
//  VALOCreateNodeOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 15.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulCreateNodeOperation)(NSDictionary *users);
typedef void(^FailedCreateNodeOperation)(NSError *error, NSDictionary *serializedData);

@interface VALOCreateNodeOperation : NSObject

- (instancetype)initWithParentID:(NSString *)parentID nodeName:(NSString *)nodeName isFile:(BOOL)isfile token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulCreateNodeOperation)success
                        failure:(FailedCreateNodeOperation)failure;

@end
