//
//  VALOGetNodeChildrenOperation.h
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetNodeChildrenNetworkOperation)(id response);
typedef void(^FailedGetNodeChildrenNetworkOperation)(NSError *error);

@interface VALOGetNodeChildrenOperation : NSObject

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulGetNodeChildrenNetworkOperation)success
                        failure:(FailedGetNodeChildrenNetworkOperation)failure;

@end
