//
//  VALOCreateDriveOperation.h
//  Valo
//
//  Created by Kirill Shalankin on 03/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulCreateDriveNetworkOperation)(NSDictionary *response);
typedef void(^FailedCreateDriveNetworkOperation)(NSError *error);

@interface VALOCreateDriveOperation : NSObject

- (instancetype)initWithDeviceID:(NSString *)deviceID driveName:(NSString *)driveName token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulCreateDriveNetworkOperation)success
                            failure:(FailedCreateDriveNetworkOperation)failure;

@end
