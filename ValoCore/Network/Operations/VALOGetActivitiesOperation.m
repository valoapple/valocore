//
//  VALOGetActivitiesOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 27.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOGetActivitiesOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"
#import "VALODeviceService.h"

@interface VALOGetActivitiesOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end


@implementation VALOGetActivitiesOperation

- (instancetype)initWithNodeID:(NSString *)nodeID fromDate:(NSString *)fromDate token:(NSString *)token {
    
    self = [super init];

    
    VALODeviceService *deviceService = [[VALODeviceService alloc] init];
    NSString *device_id = [deviceService selfDeviceId];
    if (self) {
        
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager activitiesPathString]]];
        if(fromDate) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
            NSDate *date = [dateFormatter dateFromString:fromDate];
            date = [date dateByAddingTimeInterval:1.0/1000];
            fromDate = [dateFormatter stringFromDate:date];
            _networkOperationBuilder.parameters = [@{@"node_ids" : nodeID, @"from" : fromDate, @"skip_device_id": device_id} mutableCopy];
        }
        else
            _networkOperationBuilder.parameters = [@{@"node_ids" : nodeID} mutableCopy];
    
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }

    
    
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetActivitiesOperation)success
                        failure:(FailedGetActivitiesOperation)failure
{
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        //NSLog(@"%@", response);
        success(response);
        
    } failure:^(NSError *error) {
        //NSLog(@"%@", [error localizedDescription]);
        failure(error);
    }];

}

@end
