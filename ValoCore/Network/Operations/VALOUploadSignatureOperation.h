//
//  VALOUploadSignatureOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 27.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulUploadSignatureOperation)(NSData *data);
typedef void(^FailedUploadSignatureOperation)(NSError *error);

@interface VALOUploadSignatureOperation : NSObject

- (instancetype)initWithSignature:(NSData *)signature versionID:(NSString *)versionID token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulUploadSignatureOperation)success
                        failure:(FailedUploadSignatureOperation)failure;

@end
