//
//  VALOCreateDriveOperation.m
//  Valo
//
//  Created by Kirill Shalankin on 03/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import "VALOCreateDriveOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOCreateDriveOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOCreateDriveOperation

- (instancetype)initWithDeviceID:(NSString *)deviceID driveName:(NSString *)driveName token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager drivePathString]]];
        
        _networkOperationBuilder.parameters = [@{@"device_id": deviceID,
                                                 @"kind": @"backup",
                                                 @"drive_name": driveName,
                                                 @"drive_type": @"disk"} mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
//        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulCreateDriveNetworkOperation)success
                        failure:(FailedCreateDriveNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        
        success(response);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}

@end
