//
//  VALOGetDrivesOperation.m
//  Valo
//
//  Created by Kirill Shalankin on 03/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import "VALOGetDrivesOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOGetDrivesOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOGetDrivesOperation

- (instancetype)initWithDeviceID:(NSString *)deviceID token:(NSString *)token {
    self = [super init];
    if (self) {
        if(deviceID)
            _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                        [NSString stringWithFormat:@"%@?device_id=%@",
                                         [VALONetworkURLManager drivePathString], deviceID]];
        else
            _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                        [NSString stringWithFormat:@"%@",
                                         [VALONetworkURLManager drivePathString]]];
                
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetRootNodeNetworkOperation)success
                        failure:(FailedGetRootNodeNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        success(response);
        
    } failure:^(NSError *error) {
        NSLog(@"Get root node operation ERROR:%@", [error localizedDescription]);
        failure(error);
    }];
}

@end
