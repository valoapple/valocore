//
//  VALOLoginOperation.h
//  Valo
//
//  Created by Kirill Shalankin on 14/01/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VALOUser.h"

typedef void(^SuccessfulLoginNetworkOperation)(NSDictionary *userJSON, NSString *deviceID, NSString *token);
typedef void(^FailedLoginNetworkOperation)(NSError *error, NSDictionary *serializedData);

@interface VALOLoginOperation : NSObject

- (instancetype)initWithEmail:(NSString *)email password:(NSString *)password;
- (instancetype)initWithEmail:(NSString *)email password:(NSString *)password deviceID:(NSString *)deviceID
                        token:(NSString *)token;
- (instancetype)initWithDeviceName:(NSString *)deviceName serialNumber:(NSString *)serialNumber
                              type:(NSString *)type token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulLoginNetworkOperation)success
                            failure:(FailedLoginNetworkOperation)failure;

@end
