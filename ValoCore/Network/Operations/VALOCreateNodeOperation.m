//
//  VALOCreateNodeOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 15.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOCreateNodeOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOCreateNodeOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOCreateNodeOperation

- (instancetype)initWithParentID:(NSString *)parentID nodeName:(NSString *)nodeName isFile:(BOOL)isfile token:(NSString *)token{
    self = [super init];
    if (self) {
        NSString *fileString = [NSString stringWithFormat:@"%@", isfile ? @"true" : @"false"];
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager nodePathString]]];
        
        _networkOperationBuilder.parameters = [@{@"parent_id": parentID,
                                                 @"is_file" : fileString,
                                                 @"name" : nodeName}                                             mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}


- (void)addOperationWithSuccess:(SuccessfulCreateNodeOperation)success
                        failure:(FailedCreateNodeOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        
        success(response);
    } failure:^(NSError *error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions
                                                                         error:nil];
        //NSLog(@"Login operation(failure block) JSON: %@", serializedData);
        failure(error, serializedData);

    }];

}

@end
