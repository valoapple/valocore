//
//  VALOPatchNodeOperation.h
//  ValoCore
//
//  Created by Kirill Shalankin on 27/02/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulPatchNodeNetworkOperation)(BOOL succesfulOperation);
typedef void(^FailedPatchNodeNetworkOperation)(NSError *error);

@interface VALOPatchNodeOperation : NSObject

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token name:(NSString *)name parent:(NSString *)parentID;
- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token name:(NSString *) name;
- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token delete:(BOOL) delete;

- (void)addOperationWithSuccess:(SuccessfulPatchNodeNetworkOperation)success
                        failure:(FailedPatchNodeNetworkOperation)failure;

@end
