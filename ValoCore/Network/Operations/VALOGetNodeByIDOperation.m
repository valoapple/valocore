//
//  VALOGetNodeByIDOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 20.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOGetNodeByIDOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOGetNodeByIDOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOGetNodeByIDOperation

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token
{
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [NSString stringWithFormat:[VALONetworkURLManager oneNodePathString], nodeID]]];
        
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    return self;
    
    
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetNodeByIDOperation)success
                        failure:(FailedGetNodeByIDOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        NSLog(@"%@", response);
        success(response);
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure(error);
    }];
}

@end
