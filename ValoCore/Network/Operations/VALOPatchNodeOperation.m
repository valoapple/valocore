//
//  VALOPatchNodeOperation.m
//  ValoCore
//
//  Created by Kirill Shalankin on 27/02/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOPatchNodeOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOPatchNodeOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOPatchNodeOperation

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token name:(NSString *)name parent:(NSString *)parentID {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@/%@",
                                     [VALONetworkURLManager nodePathString], nodeID]];
        
        _networkOperationBuilder.parameters = [@{@"name": name,
                                                 @"parent_id": parentID}
                                               mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPatchMethod;
    }
    return self;
}


- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token name:(NSString *) name {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@/%@",
                                     [VALONetworkURLManager nodePathString], nodeID]];
        
        _networkOperationBuilder.parameters = [@{@"name": name}
                                               mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPatchMethod;
    }
    return self;
}

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token delete:(BOOL) delete {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@/%@",
                                     [VALONetworkURLManager nodePathString], nodeID]];
        
        _networkOperationBuilder.parameters = [@{@"_delete": [NSNumber numberWithBool:delete]}
                                               mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPatchMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulPatchNodeNetworkOperation)success
                        failure:(FailedPatchNodeNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        
        success(YES);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}

@end
