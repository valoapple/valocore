//
//  VALOCreateUploadOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 15.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOCreateUploadOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOCreateUploadOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOCreateUploadOperation

- (instancetype)initWithNodeID:(NSString *)nodeID versionID:(NSString *)versionID fileSize:(NSString *)fileSize type:(NSString *)type token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager uploadPathString]]];
        if(!versionID)
        {
            _networkOperationBuilder.parameters = [@{
                                                     @"node_id": nodeID,
                                                     @"size" : fileSize,
                                                     @"type" : type}
                                                   mutableCopy];
        }
        else
        {
            _networkOperationBuilder.parameters = [@{
                                                     @"node_id": nodeID,
                                                     @"version_id" : versionID,
                                                     @"size" : fileSize,
                                                     @"type" : type}
                                                   mutableCopy];
        }
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        //[_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulCreateUploadOperation)success
                        failure:(FailedCreateUploadOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        
        success(response);
    } failure:^(NSError *error) {
        
        failure(error);
    }];

}

@end
