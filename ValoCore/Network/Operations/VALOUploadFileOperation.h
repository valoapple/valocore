//
//  VALOUploadFileOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 14.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulUploadFileOperation)(NSDictionary *files);
typedef void(^FailedUploadFileOperation)(NSError *error);

@interface VALOUploadFileOperation : NSObject

- (instancetype)initWithDeviceID:(NSString *)uploadID data:(NSData *)data offset:(NSInteger)i token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulUploadFileOperation)success
                        failure:(FailedUploadFileOperation)failure;

@end
