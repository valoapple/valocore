//
//  VALOGetNodeChildrenOperation.m
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import "VALOGetNodeChildrenOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOGetNodeChildrenOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOGetNodeChildrenOperation

- (instancetype)initWithNodeID:(NSString *)nodeID token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager nodePathString]]];
        if (nodeID.length != 0) {
            _networkOperationBuilder.parameters = [@{@"parent_id" : nodeID} mutableCopy];
        }
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetNodeChildrenNetworkOperation)success
                        failure:(FailedGetNodeChildrenNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        NSLog(@"Get node children operation JSON:%@", response);
        success(response);
        
    } failure:^(NSError *error) {
        NSLog(@"Get node children operation ERROR:%@", [error localizedDescription]);
        failure(error);
    }];
}

@end
