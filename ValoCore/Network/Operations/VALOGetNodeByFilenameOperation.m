//
//  VALOGetNodeByFilenameOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 17.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOGetNodeByFilenameOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOGetNodeByFilenameOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOGetNodeByFilenameOperation

- (instancetype)initWithParentNodeID:(NSString *)parentNodeID nodeName:(NSString *)nodeName token:(NSString *)token
{
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager nodePathString]]];
        if (parentNodeID.length != 0) {
            _networkOperationBuilder.parameters = [@{@"parent_id" : parentNodeID, @"name" : nodeName} mutableCopy];
        }
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        _networkOperationBuilder.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    return self;

    
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetNodeByFilenameOperation)success
                        failure:(FailedGetNodeByFilenameOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        NSLog(@"%@", response);
        success(response);
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure(error);
    }];
}

@end
