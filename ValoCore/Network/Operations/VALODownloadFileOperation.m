//
//  VALODownloadFileOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 14.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALODownloadFileOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALODownloadFileOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALODownloadFileOperation

- (instancetype)initWithVersionID:(NSString *)versionID type:(NSString *)type token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [NSString stringWithFormat:[VALONetworkURLManager downloadPathString], versionID,type]]];
        

       _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        _networkOperationBuilder.manager.responseSerializer = [AFCompoundResponseSerializer serializer];
        _networkOperationBuilder.manager.responseSerializer.acceptableContentTypes = [_networkOperationBuilder.manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/octet-stream"];
        //[_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulDownloadFileOperation)success
                        failure:(FailedDownloadFileOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        success(response);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}


@end
