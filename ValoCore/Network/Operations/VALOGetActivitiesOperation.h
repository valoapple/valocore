//
//  VALOGetActivitiesOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 27.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetActivitiesOperation)(id response);
typedef void(^FailedGetActivitiesOperation)(NSError *error);

@interface VALOGetActivitiesOperation : NSObject

- (instancetype)initWithNodeID:(NSString *)nodeID fromDate:(NSDate *)fromDate token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulGetActivitiesOperation)success
                        failure:(FailedGetActivitiesOperation)failure;

@end
