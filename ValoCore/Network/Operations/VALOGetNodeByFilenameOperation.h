//
//  VALOGetNodeByFilenameOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 17.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetNodeByFilenameOperation)(id result);
typedef void(^FailedGetNodeByFilenameOperation)(NSError *error);

@interface VALOGetNodeByFilenameOperation : NSObject

- (instancetype)initWithParentNodeID:(NSString *)parentNodeID nodeName:(NSString *)nodeName token:(NSString *)token;

- (void)addOperationWithSuccess:(SuccessfulGetNodeByFilenameOperation)success
                        failure:(FailedGetNodeByFilenameOperation)failure;


@end
