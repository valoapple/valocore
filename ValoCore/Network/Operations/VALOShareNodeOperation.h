//
//  VALOShareNodeOperation.h
//  ValoCore
//
//  Created by Kirill Shalankin on 11/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulShareNodeNetworkOperation)(BOOL succesfulOperation);
typedef void(^FailedShareNodeNetworkOperation)(NSError *error);

@interface VALOShareNodeOperation : NSObject

- (instancetype)initWithUserIDs:(NSArray *)userIDs token:(NSString *)token nodeID:(NSString *)nodeID flag:(BOOL)readOnly;
- (void)addOperationWithSuccess:(SuccessfulShareNodeNetworkOperation)success
                        failure:(FailedShareNodeNetworkOperation)failure;

@end
