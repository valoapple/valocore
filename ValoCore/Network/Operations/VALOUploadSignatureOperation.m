//
//  VALOUploadSignatureOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 27.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOUploadSignatureOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOUploadSignatureOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOUploadSignatureOperation

- (instancetype)initWithSignature:(NSData *)signature versionID:(NSString *)versionID token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [NSString stringWithFormat:[VALONetworkURLManager downloadPathString], versionID,@"delta"]]];
        
        
        _networkOperationBuilder.form = ^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:signature name:@"signature" fileName:@"signature" mimeType:@"application/octet-stream"];
            
        };
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.responseSerializer = [AFCompoundResponseSerializer serializer];
        _networkOperationBuilder.manager.responseSerializer.acceptableContentTypes = [_networkOperationBuilder.manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/octet-stream"];
        
        
        
        //_networkOperationBuilder.manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        
        
        
        
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        //[_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTFORMMethod;
    }
    
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    return self;
}


- (void)addOperationWithSuccess:(SuccessfulUploadSignatureOperation)success
                        failure:(FailedUploadSignatureOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        success(response);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}


@end
