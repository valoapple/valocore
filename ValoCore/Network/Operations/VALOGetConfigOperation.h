//
//  VALOGetConfigOperation.h
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetConfigNetworkOperation)(BOOL successGetConfig);
typedef void(^FailedGetConfigNetworkOperation)(NSError *error);

@interface VALOGetConfigOperation : NSObject

- (void)addOperationWithSuccess:(SuccessfulGetConfigNetworkOperation)success
                            failure:(FailedGetConfigNetworkOperation)failure;

@end
