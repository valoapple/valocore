//
//  VALOGetDrivesOperation.h
//  Valo
//
//  Created by Kirill Shalankin on 03/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulGetRootNodeNetworkOperation)(NSDictionary *drives);
typedef void(^FailedGetRootNodeNetworkOperation)(NSError *error);

@interface VALOGetDrivesOperation : NSObject

- (instancetype)initWithDeviceID:(NSString *)deviceID token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulGetRootNodeNetworkOperation)success
                            failure:(FailedGetRootNodeNetworkOperation)failure;

@end
