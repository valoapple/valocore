//
//  VALOShareNodeOperation.m
//  ValoCore
//
//  Created by Kirill Shalankin on 11/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOShareNodeOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOShareNodeOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOShareNodeOperation

- (instancetype)initWithUserIDs:(NSArray *)userIDs token:(NSString *)token nodeID:(NSString *)nodeID flag:(BOOL)readOnly {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager shareNodePathString]]];
        
        _networkOperationBuilder.parameters = [@{@"user_ids"  :userIDs,
                                                 @"node_id"   :nodeID,
                                                 @"read_only" :[NSNumber numberWithBool:readOnly]}
                                               mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        //[_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulShareNodeNetworkOperation)success
                        failure:(FailedShareNodeNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        
        success(YES);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}

@end
