//
//  VALODownloadFileOperation.h
//  ValoCore
//
//  Created by Mike Fluff on 14.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessfulDownloadFileOperation)(NSData *data);
typedef void(^FailedDownloadFileOperation)(NSError *error);

@interface VALODownloadFileOperation : NSObject

- (instancetype)initWithVersionID:(NSString *)versionID type:(NSString *)type token:(NSString *)token;
- (void)addOperationWithSuccess:(SuccessfulDownloadFileOperation)success
                        failure:(FailedDownloadFileOperation)failure;

@end
