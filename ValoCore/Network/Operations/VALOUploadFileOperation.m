 //
//  VALOUploadFileOperation.m
//  ValoCore
//
//  Created by Mike Fluff on 14.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOUploadFileOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOUploadFileOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOUploadFileOperation

- (instancetype)initWithDeviceID:(NSString *)uploadID data:(NSData *)data offset:(NSInteger)i token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager blockPathString]]];
        

        _networkOperationBuilder.form = ^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFormData:[uploadID dataUsingEncoding:NSUTF8StringEncoding] name:@"upload_id"];
            [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",(long)i] dataUsingEncoding:NSUTF8StringEncoding] name:@"offset"];
            [formData appendPartWithFileData:data name:@"data" fileName:@"valofile" mimeType:@"application/octet-stream"];
            
        };
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        
        
        
        
        //_networkOperationBuilder.manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
    
        
    
    
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        //[_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTFORMMethod;
    }
    
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    return self;
}


- (void)addOperationWithSuccess:(SuccessfulUploadFileOperation)success
                        failure:(FailedUploadFileOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        success(response);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}

@end
