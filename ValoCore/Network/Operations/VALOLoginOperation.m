//
//  VALOLoginOperation.m
//  Valo
//
//  Created by Kirill Shalankin on 14/01/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <AFNetworking.h>

#import "VALOLoginOperation.h"
#import "VALONetworkOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"
#import "VALODevice.h"
#import "VALOUserDefaultsKeyHolder.h"

@interface VALOLoginOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOLoginOperation

- (instancetype)initWithEmail:(NSString *)email password:(NSString *)password {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager loginPathString]]];
        
        _networkOperationBuilder.parameters = [@{@"email" : email,
                                                 @"password": password} mutableCopy];

        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}

- (instancetype)initWithEmail:(NSString *)email password:(NSString *)password deviceID:(NSString *)deviceID
                        token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager bindDevicePathString]]];
        
        _networkOperationBuilder.parameters = [@{@"email" : email,
                                                 @"password": password,
                                                 @"device_id": deviceID} mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}

- (instancetype)initWithDeviceName:(NSString *)deviceName serialNumber:(NSString *)serialNumber
                              type:(NSString *)type token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager registerDevicePathString]]];
        
        _networkOperationBuilder.parameters = [@{@"name": deviceName,
                                                 @"serial_number": serialNumber,
                                                 @"type": type} mutableCopy];

        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        
        _networkOperationBuilder.method = VALONetworkOperationPOSTMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulLoginNetworkOperation)success
                            failure:(FailedLoginNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        //NSLog(@"Login operation JSON: %@", response);
                
        success(response[@"user"], response[@"device"][@"id"], response[@"user"][@"token"]);
        
    } failure:^(NSError *error) {
        
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions
                                                                         error:nil];
        //NSLog(@"Login operation(failure block) JSON: %@", serializedData);
        failure(error, serializedData);
    }];
}

@end
