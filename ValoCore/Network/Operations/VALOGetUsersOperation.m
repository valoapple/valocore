//
//  VALOGetUsersOperation.m
//  ValoCore
//
//  Created by Kirill Shalankin on 11/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOGetUsersOperation.h"
#import "VALONetworkOperationBuilder.h"
#import "VALONetworkOperation.h"
#import "VALONetworkURLManager.h"
#import "VALONetworkConstants.h"

@interface VALOGetUsersOperation ()

@property (nonatomic, strong) VALONetworkOperationBuilder *networkOperationBuilder;
@property (nonatomic, strong) VALONetworkOperation *operation;

@end

@implementation VALOGetUsersOperation

- (instancetype)initWithUserIDs:(NSArray *)userIDs token:(NSString *)token {
    self = [super init];
    if (self) {
        _networkOperationBuilder = [[VALONetworkOperationBuilder alloc] initWithPath:
                                    [NSString stringWithFormat:@"%@",
                                     [VALONetworkURLManager usersPathString]]];
        
        _networkOperationBuilder.parameters = [@{@"user_ids": userIDs}
                                               mutableCopy];
        
        _networkOperationBuilder.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[self.networkOperationBuilder baseURL]];
        _networkOperationBuilder.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_networkOperationBuilder.manager.requestSerializer setValue:token forHTTPHeaderField:kVALOAccessTokenKey];
        [_networkOperationBuilder.manager.requestSerializer setValue:kVALOAcceptKey forHTTPHeaderField:kVALOAcceptType];
        
        _networkOperationBuilder.method = VALONetworkOperationGETMethod;
    }
    return self;
}

- (void)addOperationWithSuccess:(SuccessfulGetUsersNetworkOperation)success
                        failure:(FailedGetUsersNetworkOperation)failure {
    self.operation = [[VALONetworkOperation alloc] initWithBuilder:self.networkOperationBuilder];
    
    [self.operation addOperationWithSuccess:^(id response) {
        
        success(response);
    } failure:^(NSError *error) {
        
        failure(error);
    }];
}

@end
