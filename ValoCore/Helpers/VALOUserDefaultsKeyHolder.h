//
//  VALOUserDefaultsKeyHolder.h
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALOUserDefaultsKeyHolder : NSObject

+ (NSString *)suitNameKey;

+ (NSString *)configDictionaryKey;
+ (NSString *)helpDictionaryKey;
+ (NSString *)savedHostKey;
+ (NSString *)fayeDisconnectDateKey;
+ (NSString *)accessTokenKey;
+ (NSString *)deviceIDKey;
+ (NSString *)keychanUUIDKey;
+ (NSString *)showTourKey;
+ (NSString *)rootNodeIdKey;
+ (NSString *)backupNodeIdKey;
+ (NSString *)activeMenuKey;
+ (NSString *)lastActivityDate;

@end
