//
//  VALONotificationCenterNameHolder.h
//  Valo
//
//  Created by Kirill Shalankin on 09/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VALONotificationCenterNameHolder : NSObject

+ (NSString *)successfulLoginKey;
+ (NSString *)activityStopKey;
+ (NSString *)updateDatabaseKey;
+ (NSString *)updateTableKey;
+ (NSString *)newNodeListKey;
+ (NSString *)showAlertKey;
+ (NSString *)checkboxActionKey;
+ (NSString *)sharingNodeSelected;
+ (NSString *)sharingNodeUnselected;
+ (NSString *)sharingActionIsCompletedKey;
+ (NSString *)contactDidSave;
+ (NSString *)contactSelected;
+ (NSString *)contactUnselected;
+ (NSString *)contactCheckboxUpdatedKey;
+ (NSString *)nodeWasUpdatedKey;

@end
