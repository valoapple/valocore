//
//  VALOUserDefaultsKeyHolder.m
//  Valo
//
//  Created by Kirill Shalankin on 01/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import "VALOUserDefaultsKeyHolder.h"

@implementation VALOUserDefaultsKeyHolder

static NSString *const kVALOSuitName = @"group.Valo";

static NSString *const kVALOConfigDictionary    = @"keyVALOConfigDictionary";
static NSString *const kVALOHelpDictionary      = @"keyVALOHelpDictionary";
static NSString *const kVALOSavedHost           = @"keyVALOSavedHost";
static NSString *const kVALOFayeDiconnectDate   = @"keyVALOFayeDiconnectDate";
static NSString *const kVALOAccessToken         = @"keyVALOAccessToken";
static NSString *const kVALODeviceID            = @"keyVALODeviceID";
static NSString *const kVALOKeychanUUID         = @"keyVALOKeychanUUID";
static NSString *const kVALOShowTour            = @"keyVALOShowTour";
static NSString *const kVALORootNodeID          = @"keyVALORootNodeID";
static NSString *const kVALOBackupNodeID        = @"keyVALOBackupNodeID";
static NSString *const kVALOActiveMenu          = @"keyVALOActiveMenu";
static NSString *const kVALOlastActivityDate    = @"kVALOlastActivityDate";

+ (NSString *)suitNameKey {
    return kVALOSuitName;
}

+ (NSString *)configDictionaryKey {
    return kVALOConfigDictionary;
}

+ (NSString *)helpDictionaryKey {
    return kVALOHelpDictionary;
}

+ (NSString *)savedHostKey {
    return kVALOSavedHost;
}

+ (NSString *)fayeDisconnectDateKey {
    return kVALOFayeDiconnectDate;
}

+ (NSString *)accessTokenKey {
    return kVALOAccessToken;
}

+ (NSString *)deviceIDKey {
    return kVALODeviceID;
}

+ (NSString *)keychanUUIDKey {
    return kVALOKeychanUUID;
}

+ (NSString *)showTourKey {
    return kVALOShowTour;
}

+ (NSString *)rootNodeIdKey {
    return kVALORootNodeID;
}

+ (NSString *)backupNodeIdKey {
    return kVALOBackupNodeID;
}

+ (NSString *)activeMenuKey {
    return kVALOActiveMenu;
}

+ (NSString *)lastActivityDate {
    return kVALOlastActivityDate;
}

@end
