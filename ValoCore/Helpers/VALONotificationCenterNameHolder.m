//
//  VALONotificationCenterNameHolder.m
//  Valo
//
//  Created by Kirill Shalankin on 09/02/16.
//  Copyright © 2016 IQ Reserve. All rights reserved.
//

#import "VALONotificationCenterNameHolder.h"

@implementation VALONotificationCenterNameHolder

static NSString *const kVALOloginSuccessful           = @"keyVALOloginSuccessful";
static NSString *const kVALOActivityStop              = @"keyVALOActivityStop";
static NSString *const kVALOUpdateTable               = @"keyVALOUpdateTable";
static NSString *const kVALODatabaseWasUpdated        = @"keyVALODatabaseWasUpdated";
static NSString *const kVALONewNodeList               = @"keyVALONewNodeList";
static NSString *const kVALOShowAlert                 = @"keyVALOShowAlert";
static NSString *const kVALOCheckboxUpdated           = @"keyVALOCheckboxUpdated";
static NSString *const kVALOSharingNodeSelected       = @"keyVALOSharingNodeSelected";
static NSString *const kVALOSharingNodeUnselected     = @"keyVALOSharingNodeUnselected";
static NSString *const kVALOSharingActionIsCompleted  = @"keyVALOSharingActionIsCompleted";
static NSString *const kVALOContactDidSave            = @"keyVALOContactDidSave";
static NSString *const kVALOContactSelected           = @"keyVALOContactSelected";
static NSString *const kVALOContactUnselected         = @"keyVALOContactUnselected";
static NSString *const kVALOContactCheckboxUpdated    = @"keyVALOContactCheckboxUpdated";
static NSString *const kVALONodeWasUpdated            = @"keyVALONodeWasUpdated";

+ (NSString *)successfulLoginKey {
    return kVALOloginSuccessful;
}

+ (NSString *)activityStopKey {
    return kVALOActivityStop;
}

+ (NSString *)updateTableKey {
    return kVALOUpdateTable;
}

+ (NSString *)updateDatabaseKey {
    return kVALODatabaseWasUpdated;
}

+ (NSString *)newNodeListKey {
    return kVALONewNodeList;
}

+ (NSString *)showAlertKey {
    return kVALOShowAlert;
}

+ (NSString *)checkboxActionKey {
    return kVALOCheckboxUpdated;
}

+ (NSString *)sharingNodeSelected {
    return kVALOSharingNodeSelected;
}

+ (NSString *)sharingNodeUnselected {
    return kVALOSharingNodeUnselected;
}

+ (NSString *)sharingActionIsCompletedKey {
    return kVALOSharingActionIsCompleted;
}

+ (NSString *)contactDidSave {
    return kVALOContactDidSave;
}

+ (NSString *)contactSelected {
    return kVALOContactSelected;
}

+ (NSString *)contactUnselected {
    return kVALOContactUnselected;
}

+ (NSString *)contactCheckboxUpdatedKey {
    return kVALOContactCheckboxUpdated;
}

+ (NSString *)nodeWasUpdatedKey {
    return kVALONodeWasUpdated;
}

@end
