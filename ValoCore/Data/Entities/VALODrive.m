//
//  VALODrive.m
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import "VALODrive.h"
#import "VALODevice.h"
#import "VALONode.h"
#import "VALOUser.h"

@implementation VALODrive

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALODrive"];
    [mapping addAttributesFromDictionary:@{@"deviceId"       : @"device_id",
                                           @"driveName"      : @"drive_name",
                                           @"driveType"      : @"drive_type",
                                           @"id"             : @"id",
                                           @"nodeId"         : @"node_id",
                                           @"readOnly"       : @"read_only",
                                           @"driveType"      : @"drive_type",
                                           @"userId"         : @"user_id"
                                           }];
    
    //[mapping addRelationshipMapping:[VALODevice defaultMapping] forProperty:@"device" keyPath:@"device_id"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
    
    FEMAttribute *updatedAt = [[FEMAttribute alloc] initWithProperty:@"updatedAt" keyPath:@"updated_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    FEMAttribute *createdAt = [[FEMAttribute alloc] initWithProperty:@"createdAt" keyPath:@"created_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    [mapping addAttribute:updatedAt];
    [mapping addAttribute:createdAt];
    
    return mapping;
}

@end
