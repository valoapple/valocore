//
//  VALOUser+CoreDataProperties.h
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALOUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *locale;
@property (nullable, nonatomic, retain) NSString *phoneNumber;
@property (nullable, nonatomic, retain) NSNumber *usedSpace;
@property (nullable, nonatomic, retain) NSSet<VALODevice *> *devices;
@property (nullable, nonatomic, retain) NSSet<VALODrive *> *drives;

@end

@interface VALOUser (CoreDataGeneratedAccessors)

- (void)addDevicesObject:(VALODevice *)value;
- (void)removeDevicesObject:(VALODevice *)value;
- (void)addDevices:(NSSet<VALODevice *> *)values;
- (void)removeDevices:(NSSet<VALODevice *> *)values;

- (void)addDrivesObject:(VALODrive *)value;
- (void)removeDrivesObject:(VALODrive *)value;
- (void)addDrives:(NSSet<VALODrive *> *)values;
- (void)removeDrives:(NSSet<VALODrive *> *)values;

@end

NS_ASSUME_NONNULL_END
