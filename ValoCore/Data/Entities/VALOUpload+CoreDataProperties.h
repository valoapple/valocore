//
//  VALOUpload+CoreDataProperties.h
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOUpload.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALOUpload (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *offset;
@property (nullable, nonatomic, retain) NSDate *created_at;
@property (nullable, nonatomic, retain) NSDate *expires_at;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSNumber *size;
@property (nullable, nonatomic, retain) NSString *state;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSDate *updated_at;
@property (nullable, nonatomic, retain) NSString *node_id;
@property (nullable, nonatomic, retain) VALONode *node;

@end

NS_ASSUME_NONNULL_END
