//
//  VALOUser.m
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import "VALOUser.h"
#import "VALODevice.h"
#import "VALODrive.h"

@implementation VALOUser

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALOUser"];
    [mapping addAttributesFromDictionary:@{@"email"       : @"email",
                                           @"firstName"   : @"first_name",
                                           @"id"          : @"id",
                                           @"lastName"    : @"last_name",
                                           @"locale"      : @"locale",
                                           @"phoneNumber" : @"phone_number",
                                           @"usedSpace"   : @"used_space"
                                           }];
    
    return mapping;
}

@end
