//
//  VALODevice.m
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import "VALODevice.h"
#import "VALODrive.h"
#import "VALOUser.h"

@implementation VALODevice

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALODevice"];
    [mapping addAttributesFromDictionary:@{@"deviceType"       : @"device_type",
                                           @"displayName"      : @"display_name",
                                           @"id"               : @"id",
                                           @"name"             : @"name",
                                           @"userId"           : @"user_id"
                                           }];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
    
    FEMAttribute *lastConnectionDate = [[FEMAttribute alloc] initWithProperty:@"lastConnectionDate"
                                                                      keyPath:@"last_connection_date" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    [mapping addAttribute:lastConnectionDate];
    
    return mapping;
}

@end
