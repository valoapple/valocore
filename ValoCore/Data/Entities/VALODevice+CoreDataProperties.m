//
//  VALODevice+CoreDataProperties.m
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALODevice+CoreDataProperties.h"

@implementation VALODevice (CoreDataProperties)

@dynamic deviceType;
@dynamic displayName;
@dynamic id;
@dynamic name;
@dynamic userId;
@dynamic drives;
@dynamic user;
@dynamic lastConnectionDate;

@end
