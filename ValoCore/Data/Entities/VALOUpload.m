//
//  VALOUpload.m
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOUpload.h"
#import "VALONode.h"

@implementation VALOUpload

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALOVersion"];
    [mapping addAttributesFromDictionary:@{ @"offset" : @"offset",
                                            @"created_at" : @"created_at",
                                            @"expires_at" : @"expires_at",
                                            @"id" : @"id",
                                            @"node_id" : @"node_id",
                                            @"size" : @"size",
                                            @"state" : @"state",
                                            @"type" : @"type",
                                            @"updated_at" : @"updated_at",
                                            @"version_id" : @"version_id"
                                           }];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
    
    FEMAttribute *expiresdAt = [[FEMAttribute alloc] initWithProperty:@"expires_at" keyPath:@"expires_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    FEMAttribute *updatedAt = [[FEMAttribute alloc] initWithProperty:@"updated_at" keyPath:@"updated_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    FEMAttribute *createdAt = [[FEMAttribute alloc] initWithProperty:@"created_at" keyPath:@"created_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    [mapping addAttribute:updatedAt];
    [mapping addAttribute:createdAt];
    [mapping addAttribute:expiresdAt];
    
    
    
    return mapping;
}

@end
