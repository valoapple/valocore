//
//  VALOContact+CoreDataProperties.h
//  ValoCore
//
//  Created by Kirill Shalankin on 14/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOContact.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALOContact (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *avatarURL;

@end

NS_ASSUME_NONNULL_END
