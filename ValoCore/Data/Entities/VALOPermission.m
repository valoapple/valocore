//
//  VALOPermission.m
//  ValoCore
//
//  Created by Kirill Shalankin on 30/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOPermission.h"
#import "VALONode.h"

@implementation VALOPermission

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALOPermission"];
    [mapping addAttributesFromDictionary:@{@"isDeletable"     : @"is_deletable",
                                           @"isEditable"      : @"is_editable",
                                           @"isHardDeletable" : @"is_hard_deletable",
                                           @"isPreviewable"   : @"is_previewable",
                                           @"isRestorable"    : @"is_restorable",
                                           @"isShareable"     : @"is_shareable"
                                           }];
    
    return mapping;
}

@end
