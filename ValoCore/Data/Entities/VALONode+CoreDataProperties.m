//
//  VALONode+CoreDataProperties.m
//  ValoCore
//
//  Created by Kirill Shalankin on 13/04/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALONode+CoreDataProperties.h"

@implementation VALONode (CoreDataProperties)

@dynamic createdAt;
@dynamic deleted;
@dynamic deletedAt;
@dynamic id;
@dynamic isFile;
@dynamic inProcess;
@dynamic name;
@dynamic ownerId;
@dynamic parentId;
@dynamic shared;
@dynamic size;
@dynamic type;
@dynamic updatedAt;
@dynamic drive;
@dynamic permission;
@dynamic version;
@dynamic children;
@dynamic parent;

@end
