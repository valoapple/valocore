//
//  VALONode.h
//  ValoCore
//
//  Created by Kirill Shalankin on 30/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <FastEasyMapping/FastEasyMapping.h>

@class VALODrive, VALOPermission, VALOVersion;

NS_ASSUME_NONNULL_BEGIN

@interface VALONode : NSManagedObject

+ (FEMMapping *)defaultMapping;

@end

NS_ASSUME_NONNULL_END

#import "VALONode+CoreDataProperties.h"
