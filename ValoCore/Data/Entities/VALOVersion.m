//
//  VALOVersion.m
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOVersion.h"
#import "VALONode.h"

@implementation VALOVersion

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALOVersion"];
    [mapping addAttributesFromDictionary:@{@"id": @"id",
                                           @"size": @"size",
                                           @"checksum": @"checksum",
                                           @"created_at": @"created_at",
                                           @"updated_at": @"updated_at"
                                           }];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
    
    FEMAttribute *updatedAt = [[FEMAttribute alloc] initWithProperty:@"updated_at" keyPath:@"updated_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    FEMAttribute *createdAt = [[FEMAttribute alloc] initWithProperty:@"created_at" keyPath:@"created_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    [mapping addAttribute:updatedAt];
    [mapping addAttribute:createdAt];

    
    
    return mapping;
}

@end
