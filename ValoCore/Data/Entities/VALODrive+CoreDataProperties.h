//
//  VALODrive+CoreDataProperties.h
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALODrive.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALODrive (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSString *deviceId;
@property (nullable, nonatomic, retain) NSString *driveName;
@property (nullable, nonatomic, retain) NSString *driveType;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *nodeId;
@property (nullable, nonatomic, retain) NSNumber *readOnly;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSString *userId;
@property (nullable, nonatomic, retain) VALODevice *device;
@property (nullable, nonatomic, retain) VALONode *node;
@property (nullable, nonatomic, retain) VALOUser *user;

@end

NS_ASSUME_NONNULL_END
