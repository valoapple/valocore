//
//  VALOContact.m
//  ValoCore
//
//  Created by Kirill Shalankin on 14/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALOContact.h"

@implementation VALOContact

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALOContact"];
    [mapping addAttributesFromDictionary:@{@"email"       : @"email",
                                           @"firstName"   : @"first_name",
                                           @"id"          : @"id",
                                           @"lastName"    : @"last_name",
                                           @"avatarURL"   : @"avatar_url"
                                           }];
    
    return mapping;
}

@end
