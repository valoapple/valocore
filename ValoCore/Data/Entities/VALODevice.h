//
//  VALODevice.h
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <FastEasyMapping/FastEasyMapping.h>

@class VALODrive, VALOUser;

NS_ASSUME_NONNULL_BEGIN

@interface VALODevice : NSManagedObject

+ (FEMMapping *)defaultMapping;

@end

NS_ASSUME_NONNULL_END

#import "VALODevice+CoreDataProperties.h"
