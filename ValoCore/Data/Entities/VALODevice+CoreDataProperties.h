//
//  VALODevice+CoreDataProperties.h
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALODevice.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALODevice (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *deviceType;
@property (nullable, nonatomic, retain) NSString *displayName;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *userId;
@property (nullable, nonatomic, retain) NSSet<VALODrive *> *drives;
@property (nullable, nonatomic, retain) VALOUser *user;
@property (nullable, nonatomic, retain) NSDate *lastConnectionDate;

@end

@interface VALODevice (CoreDataGeneratedAccessors)

- (void)addDrivesObject:(VALODrive *)value;
- (void)removeDrivesObject:(VALODrive *)value;
- (void)addDrives:(NSSet<VALODrive *> *)values;
- (void)removeDrives:(NSSet<VALODrive *> *)values;

@end

NS_ASSUME_NONNULL_END
