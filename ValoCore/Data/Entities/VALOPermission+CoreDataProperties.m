//
//  VALOPermission+CoreDataProperties.m
//  ValoCore
//
//  Created by Kirill Shalankin on 30/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOPermission+CoreDataProperties.h"

@implementation VALOPermission (CoreDataProperties)

@dynamic isDeletable;
@dynamic isEditable;
@dynamic isHardDeletable;
@dynamic isPreviewable;
@dynamic isRestorable;
@dynamic isShareable;
@dynamic node;

@end
