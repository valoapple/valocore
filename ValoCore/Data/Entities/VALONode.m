//
//  VALONode.m
//  ValoCore
//
//  Created by Kirill Shalankin on 30/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import "VALONode.h"
#import "VALODrive.h"
#import "VALOPermission.h"
#import "VALOVersion.h"

@implementation VALONode

+ (FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:@"VALONode"];
    [mapping addAttributesFromDictionary:@{@"name"      : @"name",
                                           @"id"        : @"id",
                                           @"ownerId"   : @"owner_id",
                                           @"parentId"  : @"parent_id",
                                           @"isFile"    : @"is_file",
                                           @"shared"    : @"has_shares",
                                           @"size"      : @"size",
                                           @"deleted"   : @"is_deleted"
                                           }];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-M-dd'T'HH:mm:ss.SSSZ";
    
    FEMAttribute *updatedAt = [[FEMAttribute alloc] initWithProperty:@"updatedAt" keyPath:@"updated_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    FEMAttribute *createdAt = [[FEMAttribute alloc] initWithProperty:@"createdAt" keyPath:@"created_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    FEMAttribute *deletedAt = [[FEMAttribute alloc] initWithProperty:@"deletedAt" keyPath:@"deleted_at" map:^id(id value) {
        if ([value isKindOfClass:[NSString class]]) {
            return [dateFormatter dateFromString:value];
        }
        return nil;
    } reverseMap:^id(id value) {
        return [dateFormatter stringFromDate:value];
    }];
    
    [mapping addAttribute:updatedAt];
    [mapping addAttribute:createdAt];
    [mapping addAttribute:deletedAt];
    
    [mapping addRelationshipMapping:[VALOVersion defaultMapping] forProperty:@"version" keyPath:@"current_version"];
    [mapping addRelationshipMapping:[VALOPermission defaultMapping] forProperty:@"permission" keyPath:@"permissions"];
    
    return mapping;
}

@end
