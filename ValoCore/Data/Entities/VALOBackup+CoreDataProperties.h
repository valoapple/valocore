//
//  VALOBackup+CoreDataProperties.h
//  ValoCore
//
//  Created by Mike Fluff on 17.04.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOBackup.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALOBackup (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSNumber *active;
@property (nullable, nonatomic, retain) NSNumber *filesInside;
@property (nullable, nonatomic, retain) VALONode *node;

@end

NS_ASSUME_NONNULL_END
