//
//  VALOUser+CoreDataProperties.m
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOUser+CoreDataProperties.h"

@implementation VALOUser (CoreDataProperties)

@dynamic email;
@dynamic firstName;
@dynamic id;
@dynamic lastName;
@dynamic locale;
@dynamic phoneNumber;
@dynamic usedSpace;
@dynamic devices;
@dynamic drives;

@end
