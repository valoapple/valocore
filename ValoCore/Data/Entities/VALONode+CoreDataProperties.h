//
//  VALONode+CoreDataProperties.h
//  ValoCore
//
//  Created by Kirill Shalankin on 13/04/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALONode.h"

typedef enum Types : int16_t {
    kVALODocumentFileType     = 9,
    kVALOVideoFileType        = 1,
    kVALOAudioFileType        = 2,
    kVALOPDFFileType          = 3,
    kVALOSpreadsheetFileType  = 4,
    kVALOPresentationFileType = 5,
    kVALOTextFileType         = 6,
    kVALOContactCardFileType  = 7,
    kVALOImageFileType        = 8,
    kVALOFolderType           = 10,
    kVALODefaultFileType      = 0
} fileType;

NS_ASSUME_NONNULL_BEGIN

@interface VALONode (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSNumber *deleted;
@property (nullable, nonatomic, retain) NSDate *deletedAt;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSNumber *isFile;
@property (nullable, nonatomic, retain) NSNumber *inProcess;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *ownerId;
@property (nullable, nonatomic, retain) NSString *parentId;
@property (nullable, nonatomic, retain) NSNumber *shared;
@property (nullable, nonatomic, retain) NSNumber *size;
@property (nonatomic) fileType type;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) VALODrive *drive;
@property (nullable, nonatomic, retain) VALOPermission *permission;
@property (nullable, nonatomic, retain) VALOVersion *version;
@property (nullable, nonatomic, retain) NSSet<VALONode *> *children;
@property (nullable, nonatomic, retain) VALONode *parent;

@end

@interface VALONode (CoreDataGeneratedAccessors)

- (void)addChildrenObject:(VALONode *)value;
- (void)removeChildrenObject:(VALONode *)value;
- (void)addChildren:(NSSet<VALONode *> *)values;
- (void)removeChildren:(NSSet<VALONode *> *)values;

@end

NS_ASSUME_NONNULL_END
