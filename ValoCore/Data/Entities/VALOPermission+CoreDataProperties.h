//
//  VALOPermission+CoreDataProperties.h
//  ValoCore
//
//  Created by Kirill Shalankin on 30/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOPermission.h"

NS_ASSUME_NONNULL_BEGIN

@interface VALOPermission (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *isDeletable;
@property (nullable, nonatomic, retain) NSNumber *isEditable;
@property (nullable, nonatomic, retain) NSNumber *isHardDeletable;
@property (nullable, nonatomic, retain) NSNumber *isPreviewable;
@property (nullable, nonatomic, retain) NSNumber *isRestorable;
@property (nullable, nonatomic, retain) NSNumber *isShareable;
@property (nullable, nonatomic, retain) VALONode *node;

@end

NS_ASSUME_NONNULL_END
