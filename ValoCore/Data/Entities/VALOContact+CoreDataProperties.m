//
//  VALOContact+CoreDataProperties.m
//  ValoCore
//
//  Created by Kirill Shalankin on 14/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOContact+CoreDataProperties.h"

@implementation VALOContact (CoreDataProperties)

@dynamic firstName;
@dynamic lastName;
@dynamic email;
@dynamic id;
@dynamic avatarURL;

@end
