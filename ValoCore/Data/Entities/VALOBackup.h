//
//  VALOBackup.h
//  ValoCore
//
//  Created by Mike Fluff on 17.04.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VALONode;

NS_ASSUME_NONNULL_BEGIN

@interface VALOBackup : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "VALOBackup+CoreDataProperties.h"
