//
//  VALOVersion+CoreDataProperties.m
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOVersion+CoreDataProperties.h"

@implementation VALOVersion (CoreDataProperties)

@dynamic checksum;
@dynamic size;
@dynamic id;
@dynamic meta_data;
@dynamic created_at;
@dynamic updated_at;
@dynamic node;

@end
