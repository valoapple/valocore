//
//  VALOPermission.h
//  ValoCore
//
//  Created by Kirill Shalankin on 30/03/16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <FastEasyMapping/FastEasyMapping.h>

@class VALONode;

NS_ASSUME_NONNULL_BEGIN

@interface VALOPermission : NSManagedObject

+ (FEMMapping *)defaultMapping;

@end

NS_ASSUME_NONNULL_END

#import "VALOPermission+CoreDataProperties.h"
