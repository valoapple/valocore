//
//  VALOUpload+CoreDataProperties.m
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOUpload+CoreDataProperties.h"

@implementation VALOUpload (CoreDataProperties)

@dynamic offset;
@dynamic created_at;
@dynamic expires_at;
@dynamic id;
@dynamic size;
@dynamic state;
@dynamic type;
@dynamic updated_at;
@dynamic node_id;
@dynamic node;

@end
