//
//  VALOBackup+CoreDataProperties.m
//  ValoCore
//
//  Created by Mike Fluff on 17.04.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALOBackup+CoreDataProperties.h"

@implementation VALOBackup (CoreDataProperties)

@dynamic path;
@dynamic active;
@dynamic filesInside;
@dynamic node;

@end
