//
//  VALOUpload.h
//  ValoCore
//
//  Created by Mike Fluff on 21.03.16.
//  Copyright © 2016 IQReserve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <FastEasyMapping/FastEasyMapping.h>

@class VALONode;

NS_ASSUME_NONNULL_BEGIN

@interface VALOUpload : NSManagedObject

+ (FEMMapping *)defaultMapping;

@end

NS_ASSUME_NONNULL_END

#import "VALOUpload+CoreDataProperties.h"
