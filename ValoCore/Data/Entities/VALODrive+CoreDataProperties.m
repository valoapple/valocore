//
//  VALODrive+CoreDataProperties.m
//  ValoCore
//
//  Created by Kirill Shalankin on 25/02/16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VALODrive+CoreDataProperties.h"

@implementation VALODrive (CoreDataProperties)

@dynamic createdAt;
@dynamic deviceId;
@dynamic driveName;
@dynamic driveType;
@dynamic id;
@dynamic nodeId;
@dynamic readOnly;
@dynamic updatedAt;
@dynamic userId;
@dynamic device;
@dynamic node;
@dynamic user;

@end
